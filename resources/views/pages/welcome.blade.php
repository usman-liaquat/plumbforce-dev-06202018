<!DOCTYPE html>
   <html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width,initial-scale=1">

					 <!-- title -->
		        <title>Plumb Force</title>
				<link rel="shortcut icon" href="">
				
				<link rel="stylesheet" href="{{asset('homepage/css/font-awesome/css/font-awesome.min.css')}}">
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			         <!-- Bootstrap links-->
			<link rel="stylesheet" href="{{asset('homepage/css/bootstrap.min.css')}}">
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		   <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
            <!-- Custom Style -->
			<link rel="stylesheet" href="{{asset('homepage/css/plumbforce.css')}}">
            <link rel="stylesheet" href="{{asset('homepage/css/animate.css')}}">			
		</head>
		<body>
		  <header> <!-- header-start  -->
	        <div class="container">
			  <div class="row">
		        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				   <div class="subheader">
			         <h3>01384 351085</h3>
					</div>
			    </div>
			   </div>
			   <div class="row">
			     <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				   <div class="logo">
				    <img src="{{asset('homepage/images/logo.png')}}">
				 </div>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				    <div class="nav-mid">
					  <nav>
					     <li><a href="#">HOME</a></li>
						 <li><a href="#">SERVICES</a></li>
						 <li><a href="#">REVIEWS</a></li>
						 <li><a href="#">CONTACT</a></li>
					  </nav>
					</div>
				 </div>
				 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				    <div class="nav-rite">
					  <nav>
						
					@if (Auth::check())
					     <li><a href="{{route('blog.index')}}"> {{ Auth::user()->name }} Dashboard</a></li>
						 <li><a href="{{ route('logout') }}">Logout</a></li>
					@else
						<li><a href="{{ route('login') }}">LOGIN</a></li>
						<li><a href="{{url('/auth/register')}}">REGISTER</a></li>
					@endif
			<!-- <li><a href="#"><button type="button" class="btn btn-default">Get a quote</button></a></li> -->
					  </nav>
					</div>
				 </div>
			   </div>
			</div>
		  </header> <!-- Header-closed -->
		 
		  <section class="plumb-force"> <!-- plumb-force-start -->
		    <div class="container">
			  <div class="row">
			       <div class="blue-box">
				      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					     <div class="blue-box-left">
						     <h3>PLUMB FORCE 
                               heating and
                               <span>plumbing experts</span></h3>
							  <p>Plumb-force are Worcester Bosch approved installers, along with Vaillant, Baxi and other reputable manufacturers. We provide services for aspects of plumbing, heating and more. We have carried out gas and plumbing work for over 10 years in the West Midlands and beyond.</p> 
						 </div>
					  </div>
					  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4">
					      <div class="blue-box-right">
					      <div class="row">
						    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							  <div class="img-col">
							    <img src="{{asset('homepage/images/cooler.png')}}">
							    <h5>Boilers</h5>
							   </div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							  <div class="img-col">
							    <img src="{{asset('homepage/images/heater.png')}}">
							    <h5>Heating</h5>
							   </div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							   <div class="img-col">
							    <img src="{{asset('homepage/images/gas.png')}}">
							    <h5>Gas Fires</h5>
							   </div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							  <div class="img-col">
							    <img src="{{asset('homepage/images/tap.png')}}">
							    <h5>Generic
                                  <span>Installing</span></h5>
							  </div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							  <div class="img-col">
							    <img src="{{asset('homepage/images/abc.png')}}">
							    <h5>Water
								 <span>Heaters</span></h5>
							  </div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
							   <div class="img-col">
							   <img src="{{asset('homepage/images/abc1.png')}}">
							   <h5>Under Floor
								<span>Heating</span></h5>
								 </div>
							</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					 <div class="btn-col">
						<a href="#"><button type="button" class="btn btn-default">View More</button></a>
				     </div>
					</div>
						  </div>
					   </div>
					  </div>
				   </div>
			  </div>
			</div>
		  </section> <!-- plumb-force-closed-->
		  
		  
		   <section class="Why-choose-us"><!--Why-choose-us-->
		      <div class="container">
			    <div class="row">
				   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				     <div class="why-choose-us-left">
				        <h3>Why Choose Us</h3>
						<h5>Our engineers arrive when they say they will arrive.</h5>
						<ul>
						  <li><img src="{{asset('homepage/images/reward.png')}}"><p>Free 5 year warranty for parts and labour</p></li>
						  <li><img src="{{asset('homepage/images/file.png')}}"><p>Special Plumbforce are reputable and approved installers of 
                          <span>Vaillant and Worcester Bosch boilers</span></p></li>
						  <li><img src="{{asset('homepage/images/percent.png')}}"><p>10% dicount for OAPs</p></li>
						</ul>
				     </div>
				   </div>
				   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				     <div class="why-choose-us-right">
					   <img src="{{asset('homepage/images/plumber.png')}}">
					 </div>
				   </div>
				</div>
				</div>
			 <div class="bground">
			   <div class="container-fluid">
				<div class="row">
				   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				      <div class="free-quote">
					    <article>
					    <h2>Free quote</h2>
						<p>Please use the form below to send us a message. Please include as much detail 
                        as is necessary for us to help effectively with your query.</p>
						  <input type="text" placeholder="First Name">
						  <input type="text" placeholder="Phone Number">
						  <textarea class="form-control email" rows="1" id="comment">Email</textarea>
						  <textarea class="form-control message" rows="1" id="comment">Message</textarea>
					 <a href="#"><button type="button" class="btn btn-default">Read More</button></a>
					    </article>
					  </div>
				   </div>
				</div>
			</div>	
			  </div>
		   </section><!-- why-choose-us-closed -->
		   
		   <section class="What-client-say"><!-- What-client-say-->
		      <div class="container">
			    <div class="row">
			     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				   <div class="title">
				    <h3>What client says about us
                        <span>Lorem Ipsum is simply dummy text</span></h3>
				   </div>
				 </div>
				</div> 
				<div class="row">
				  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				    <div class="client-section">
				      <img src="{{asset('homepage/images/client1.png')}}">
					  <h3>Reynold D'Silva
                        <span>co founder clarity studio</span></h3>
						<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
						<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>
					</div>
				  </div>
				  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				     <div class="client-section">
				        <img src="{{asset('homepage/images/client2.png')}}">
						  <h3>John Smith
                        <span>co founder clarity studio</span></h3>
						<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
						<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,<p>
					 </div>	
				  </div>
				  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				    <div class="client-section">
				          <img src="{{asset('homepage/images/client3.png')}}">
						    <h3>D'Silva
                        <span>co founder clarity studio</span></h3>
						<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
						<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>
				    </div>
				  </div>
				</div>
			  </div>
              <div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<p>
	We are based in the West Midlands, but are willing to travel
to anywhere in the UK for larger jobs and subject to certain
conditions. Please call us for more details.

</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
<h4>
	Contact us for<br>
Free quote
	<span><img src="{{asset('homepage/images/abc_06.png')}}" alt=""> 01384 351085</span>
</h4>
					</div>
				</div>
			</div>
		</div>
		   </section><!-- What-client-say-closed-->
           
           <div class="footer">
<div class="container">
<div class="row">
<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
<div class="f-left-area border-right">
<a href="#"><img src="{{asset('homepage/images/logo.png')}}" alt=""></a>
<p>We have built a reputation based upon trust, 
professionalism and the recommendations
made by our satisfied customers.</p>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
<div class="f-center-area">
<h6><i class="fa fa-map-marker" aria-hidden="true"></i>16 North St, Dudley, DY2 7DU</h6>
<h6><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:01384351085">Telephone:   01384 351085 </a><br>
<a href="tel:07506704749">Mobile: 	  07506 704 749 </a></h6>
<h6><i class="fa fa-envelope" aria-hidden="true"></i><a href="info@plumbforece.co.uk">info@plumbforece.co.uk</a></h6>
<h5><span>Follow Us:</span> <a href="#"> <img src="{{asset('homepage/images/f.png')}}" alt=""></a> <a href="#"> <img src="{{asset('homepage/images/f2.png')}}" alt=""></a></h5>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
<div class="f-right-area">
<h6>Sign up with your email address to receive
news and updates.</h6>
<div class="n-form">
<form>
<input type="email" name="email" placeholder="Email">
<input type="submit" value="Sign up now">
</form>
</div>

</div>
</div>

<div style="clear:both"></div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="f-cen-area text-center">
<p>Copyright © 2018 plumb-force</p>
</div>
</div>


</div>
</div>
</div>
		   
			 <!-- Jquery  -->
		 <script src="{{asset('homepage/js/jquery.js')}}"></script>
		 <script src="{{asset('homepage/js/wow.min.js')}}"></script>
		 <script src="{{asset('homepage/js/custom.js')}}"></script>
		 <script src="{{asset('homepage/js/bootstrap.min.js')}}"></script>
	 </body>
	 </html>
	
						