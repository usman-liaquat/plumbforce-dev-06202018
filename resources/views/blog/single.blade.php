@extends('main')
<?php $titleTag = htmlspecialchars($post->title); ?>
@section('title', "| $titleTag")
@section('stylesheets')

	{!! Html::style('css/parsley.css') !!}
	{!! Html::style('css/select2.min.css') !!}
	<link rel="stylesheet" href="{{asset('css/dropzone.css')}}">
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

	<script>
		tinymce.init({
			selector: 'textarea',
			plugins: 'link code',
			menubar: false
		});
	</script>

@endsection
@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			@if(!empty($post->image))
				<img src="{{asset('/images/' . $post->image)}}" width="800" height="400" />
			@endif
			<h2>{{ $post->title }}</h2>
			<hr>
			<h3>Description</h3>
			<p>{!! $post->body !!}</p>
			<hr>
			<p>Job Posted In: {{ $post->category->name }}</p>
			@include('posts.templates.questionsTemplate');
		</div>
	
	</div>
@if($post->status == 5)
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3 class="comments-title"><span class="glyphicon glyphicon-comment"></span>  {{ $post->comments()->count() }} Reviews</h3>
			@foreach($post->comments as $comment)
				<div class="comment">
					<div class="author-info">

						<img src="{{ "https://www.gravatar.com/avatar/" . md5(strtolower(trim($comment->email))) . "?s=50&d=monsterid" }}" class="author-image">
						<div class="author-name">
							<h4>{{ $comment->name }}</h4>
							<p class="author-time">{{ date('F dS, Y - g:iA' ,strtotime($comment->created_at)) }}</p>
						</div>

					</div>

					<div class="comment-content">
						{{ $comment->comment }}
					</div>

				</div>
			@endforeach
		</div>
	</div>
@if(Auth::user())
	@if($post->user_id == Auth::user()->id)
		<div class="row">
			<div id="comment-form" class="col-md-8 col-md-offset-2" style="margin-top: 50px;">
				{{ Form::open(['route' => ['comments.store', $post->id], 'method' => 'POST']) }}

					<div class="row">
					<input type="hidden" value="{{Auth::User()->id}}" name="id">
						<div class="col-md-6" hidden>
							{{ Form::label('name', "Name:") }}
							{{ Form::text('name', Auth::User()->name, ['class' => 'form-control']) }}
						</div>

						<div class="col-md-6" hidden>
							{{ Form::label('email', 'Email:') }}
							{{ Form::text('email', Auth::User()->email, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-12">
							{{ Form::label('comment', "Post Review:") }}
							{{ Form::textarea('comment', null, ['class' => 'form-control', 'rows' => '5']) }}

							{{ Form::submit('Add Review', ['class' => 'btn btn-success btn-block', 'style' => 'margin-top:15px;']) }}
						</div>
					</div>

				{{ Form::close() }}
			</div>
		</div>
	@endif
@endif
@endif
@endsection
@section('scripts')
<script>
var postRoomsDimensions = [];
    @if($postRoomsDimensions_array)
    postRoomsDimensions = '{{json_encode($postRoomsDimensions_array)}}';
	postRoomsDimensions = JSON.parse(postRoomsDimensions.replace(/&quot;/g,'"'));  
	console.log("Rooms Dimensions",postRoomsDimensions);
    @else
console.log("dd")
	@endif
	for(var x=0; x < postRoomsDimensions.length; x++){
        $('#room_dimentions').append(`
            <label for="validationCustom02">Room `+x+`: Details</label>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Length</label>
                        <input type="number" min=0 class="form-control" id="length" name="room_dimensions[length][]" value=`+postRoomsDimensions[x]['length']+` placeholder="Length" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Height</label>
                        <input type="number" min=0 class="form-control" id="height" name="room_dimensions[height][]" value=`+postRoomsDimensions[x]['height']+` placeholder="Height" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Width</label>
                        <input type="number" min=0 class="form-control" id="width" name="room_dimensions[width][]" value=`+postRoomsDimensions[x]['width']+` placeholder="Width" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Description</label>
                        <textarea type="text" class="form-control" rows="3" id="description" name="room_dimensions[description][]" placeholder="Description" required>`+postRoomsDimensions[x]['description']+`</textarea>
                    </div>
                </div>
            `);
    }
var title = "{{$post->title}};";
var postal_code = "{{$post->postal_code}};";
var address = "{{$post->address}};"

var q1=JSON.parse("{{ json_encode($post->q1) }}".replace(/&quot;/g,'"'));
var q2=JSON.parse("{{ json_encode($post->q2) }}".replace(/&quot;/g,'"'));
var q3=JSON.parse("{{ json_encode($post->q3) }}".replace(/&quot;/g,'"'));
var q4=JSON.parse("{{ json_encode($post->q4) }}".replace(/&quot;/g,'"'));
var q5=JSON.parse("{{ json_encode($post->q5) }}".replace(/&quot;/g,'"'));
var q6=JSON.parse("{{ json_encode($post->q6) }}".replace(/&quot;/g,'"'));
var q7=JSON.parse("{{ json_encode($post->q7) }}".replace(/&quot;/g,'"'));
var q8=JSON.parse("{{ json_encode($post->q8) }}".replace(/&quot;/g,'"'));
var q9=JSON.parse("{{ json_encode($post->q9) }}".replace(/&quot;/g,'"'));
var q10=JSON.parse("{{ json_encode($post->q10) }}".replace(/&quot;/g,'"'));
var q11=JSON.parse("{{ json_encode($post->q11) }}".replace(/&quot;/g,'"'));
var q12=JSON.parse("{{ json_encode($post->q12) }}".replace(/&quot;/g,'"'));
var q13=JSON.parse("{{ json_encode($post->q13) }}".replace(/&quot;/g,'"'));
</script>
<script src="{{asset('/js/checkbox_values.js')}}"></script>
@endsection