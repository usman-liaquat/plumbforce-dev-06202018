@extends('main')

@section('title', '| Create New Post')

@section('stylesheets')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	{!! Html::style('css/parsley.css') !!}
	{!! Html::style('css/select2.min.css') !!}
	<link rel="stylesheet" href="{{asset('css/dropzone.css')}}">
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

<script>
		tinymce.init({
			selector: 'textarea',
			plugins: 'link code',
			menubar: false
		});

$(document).ready(function() {
	var limit = 8;
	var i = 1;
	$(".btn-success").click(function(){
		if (i < limit){ 
			var html = $(".clone").html();
			$(".increment").after(html);
			i++;
		}
		else{
			alert("Limit Excced");
		}
	});

	$("body").on("click",".btn-danger",function(){ 
		$(this).parents(".control-group").remove();
		i--;
	});

});
</script>

@endsection

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Create New Post</h1>
			<hr>
			<!-- <form id="create-post-form" class="data-parsley-validate"> -->
			{!! Form::open(array( 'id'=>'create-post-form' ,'route' => 'posts.store',  'data-parsley-validate' => '', 'files' => 'true', 'enctype' => 'multipart/form-data')) !!}
				<!-- {{ Form::label('title', 'Title:') }}
				{{ Form::text('title', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}  -->

				<!-- {{ Form::label('slug', 'Slug:') }}
				{{ Form::text('slug', null, array('class' => 'form-control', 'required' => '', 'minlength' => '5', 'maxlength' => '255') ) }} -->

				{{ Form::label('category_id', 'Category:') }}
				<select class="form-control" name="category_id">
					@foreach($categories as $category)
						<option value='{{ $category->id }}'>{{ $category->name }}</option>
					@endforeach

				</select>
				<input type="hidden" name="user_id" value="{{Auth::user()->id}}">

				<!-- {{ Form::label('tags', 'Tags:') }}
				<select class="form-control select2-multi" name="tags[]" multiple="multiple">
					@foreach($tags as $tag)
						<option value='{{ $tag->id }}'>{{ $tag->name }}</option>
					@endforeach

				</select> -->
				<div class="input-group control-group increment" >
					<input type="file" name="filename[]" class="form-control">
					<div class="input-group-btn"> 
						<button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
					</div>
					</div>
					<div class="clone hide">
					<div class="control-group input-group" style="margin-top:10px">
						<input type="file" name="filename[]" class="form-control">
						<div class="input-group-btn"> 
						<button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
						</div>
					</div>
				</div>
				
				<!-- <div class="col-md-12">
				{{ Form::label('featured_img', 'Upload a Featured Image') }}
				{{ Form::file('featured_img') }}
				</div> -->
				<!-- <div class="col-md-12">
				{{ Form::label('featured_img', 'Upload a Featured Image') }}
				{{ Form::file('featured_img[]') }}
				</div> -->
				<?php
				$post = null;
				?>
				@include('posts.templates.questionsTemplate');
				
				{{ Form::submit('Create Post', array('id'=>'submit-all', 'class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
			{!! Form::close() !!}
		</div>
	</div>

@endsection


@section('scripts')

	{!! Html::script('js/parsley.min.js') !!}
	{!! Html::script('js/select2.min.js') !!}

	<script type="text/javascript">
		$('input').on('blur', function() {
			if ($("#create-post-form").valid()) {
				$('#submit-all').prop('disabled', false);  
			} else {
				$('#submit-all').prop('disabled', 'disabled');
			}
		});
		$('.select2-multi').select2();
	
		var total_photos_counter = 0;
		Dropzone.options.myDropzone = {
			// Prevents Dropzone from uploading dropped files immediately
  			autoProcessQueue: false,
			uploadMultiple: true,
			parallelUploads: 2,
			maxFilesize: 10,
			previewTemplate: document.querySelector('#preview').innerHTML,
			addRemoveLinks: true,
			dictRemoveFile: 'Remove file',
			dictFileTooBig: 'Image is larger than 16MB',
			timeout: 10000,

			init: function () {
				var submitButton = document.querySelector("#submit-all");
				myDropzone = this; // closure
				submitButton.addEventListener("click", function() {
				myDropzone.processQueue(); // Tell Dropzone to process all queued files.
				});
				this.on("removedfile", function (file) {
					$.post({
						url: '/images-delete',
						data: {id: file.name, _token: $('[name="_token"]').val()},
						dataType: 'json',
						success: function (data) {
							total_photos_counter--;
							$("#counter").text("# " + total_photos_counter);
						}
					});
				});
			},
			success: function (file, done) {
				total_photos_counter++;
				$("#counter").text("# " + total_photos_counter);
			}
		};
	</script>
	
@endsection
