@extends('main')

@section('title', '| Edit Job Post')

@section('stylesheets')

	{!! Html::style('css/select2.min.css') !!}

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>


	<script>
		tinymce.init({
			selector: 'textarea',
			plugins: 'link code',
			menubar: false
		});
	</script>

@endsection

@section('content')

	<div class="row">
		{!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT']) !!}
		<div class="col-md-8">
			<!-- {{ Form::label('title', 'Title:') }}
			{{ Form::text('title', null, ["class" => 'form-control input-lg']) }} -->

			<!-- {{ Form::label('slug', 'Slug:', ['class' => 'form-spacing-top']) }}
			{{ Form::text('slug', null, ['class' => 'form-control','hidden']) }} -->

			{{ Form::label('category_id', "Category:", ['class' => 'form-spacing-top']) }}
			{{ Form::select('category_id', $categories, null, ['class' => 'form-control']) }}

			<!-- {{ Form::label('tags', 'Tags:', ['class' => 'form-spacing-top']) }}
			{{ Form::select('tags[]', $tags, null, ['class' => 'form-control select2-multi', 'multiple' => 'multiple']) }} -->
			
			<!-- {{ Form::label('body', "Body:", ['class' => 'form-spacing-top']) }}
			{{ Form::textarea('body', null, ['class' => 'form-control']) }} -->
		@include('posts.templates.questionsTemplate');

		</div>
		<div class="col-md-4">
			<div class="well">
				<dl class="dl-horizontal">
					<dt>Created At:</dt>

					<dd>{{ date('M j, Y h:ia', strtotime($post->created_at)) }}</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Last Updated:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($post->updated_at)) }}</dd>
				</dl>
				<hr>
				<dl class="dl-horizontal">
					<dt>Status:</dt>
					<dd>
					@foreach($postStatus as $status)
					@if($status->id == $post->status)
					<input type="radio" name="status" id="status" value="{{$status->id}}" checked="checked">
					@else
					<input type="radio" name="status" id="status" value="{{$status->id}}">
					@endif
					{{$status->name}} <br>
					@endforeach
					</dd>
				</dl>
				<hr>
				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('posts.show', 'Cancel', array($post->id), array('class' => 'btn btn-danger btn-block')) !!}
					</div>
					<div class="col-sm-6">
						{{ Form::submit('Save Changes', ['class' => 'btn btn-success btn-block']) }}
					</div>
				</div>

			</div>
		</div>
		{!! Form::close() !!}
	</div>	<!-- end of .row (form) -->

@stop

@section('scripts')

	{!! Html::script('js/select2.min.js') !!}

<script type="text/javascript">

	$('.select2-multi').select2();
	$('.select2-multi').select2().val({!! json_encode($post->tags()->getRelatedIds()) !!}).trigger('change');
	var postRoomsDimensions = [];
    @if($postRoomsDimensions_array)
    postRoomsDimensions = '{{json_encode($postRoomsDimensions_array)}}';
	postRoomsDimensions = JSON.parse(postRoomsDimensions.replace(/&quot;/g,'"'));
	// console.log(postRoomsDimensions);  
	@else
	// alert();
	@endif
	for(var x=0; x < postRoomsDimensions.length; x++){
        $('#room_dimentions').append(`
            <label for="validationCustom02">Room `+x+`: Details</label>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Length</label>
                        <input type="number" min=0 class="form-control" id="length" name="room_dimensions[length][]" value=`+postRoomsDimensions[x]['length']+` placeholder="Length" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Height</label>
                        <input type="number" min=0 class="form-control" id="height" name="room_dimensions[height][]" value=`+postRoomsDimensions[x]['height']+` placeholder="Height" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Width</label>
                        <input type="number" min=0 class="form-control" id="width" name="room_dimensions[width][]" value=`+postRoomsDimensions[x]['width']+` placeholder="Width" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Description</label>
                        <textarea type="text" class="form-control" rows="3" id="description" name="room_dimensions[description][]" placeholder="Description" required>`+postRoomsDimensions[x]['description']+`</textarea>
                    </div>
                </div>
            `);
    }

var title = "{{$post->title}};";
var postal_code = "{{$post->postal_code}};";
var address = "{{$post->address}};"

var q1=JSON.parse("{{ json_encode($post->q1) }}".replace(/&quot;/g,'"'));
var q2=JSON.parse("{{ json_encode($post->q2) }}".replace(/&quot;/g,'"'));
var q3=JSON.parse("{{ json_encode($post->q3) }}".replace(/&quot;/g,'"'));
var q4=JSON.parse("{{ json_encode($post->q4) }}".replace(/&quot;/g,'"'));
var q5=JSON.parse("{{ json_encode($post->q5) }}".replace(/&quot;/g,'"'));
var q6=JSON.parse("{{ json_encode($post->q6) }}".replace(/&quot;/g,'"'));
var q7=JSON.parse("{{ json_encode($post->q7) }}".replace(/&quot;/g,'"'));
var q8=JSON.parse("{{ json_encode($post->q8) }}".replace(/&quot;/g,'"'));
var q9=JSON.parse("{{ json_encode($post->q9) }}".replace(/&quot;/g,'"'));
var q10=JSON.parse("{{ json_encode($post->q10) }}".replace(/&quot;/g,'"'));
var q11=JSON.parse("{{ json_encode($post->q11) }}".replace(/&quot;/g,'"'));
var q12=JSON.parse("{{ json_encode($post->q12) }}".replace(/&quot;/g,'"'));
var q13=JSON.parse("{{ json_encode($post->q13) }}".replace(/&quot;/g,'"'));
</script>
<script src="{{asset('/js/checkbox_values.js')}}"></script>

@endsection