@extends('main')

@section('title', '| View Post')

@section('stylesheets')
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  

{!! Html::style('css/parsley.css') !!}
{!! Html::style('css/select2.min.css') !!}
<link rel="stylesheet" href="{{asset('css/dropzone.css')}}">
<link rel="stylesheet" href="{{asset('css/quote-template.css')}}">

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

<script>
	tinymce.init({
		selector: 'textarea',
		plugins: 'link code',
		menubar: false
	});
</script>

@endsection

@section('content')

	<div class="row">
		<div class="col-md-8">
			<h1>{{ $post->title }}</h1>
			<hr>
			<h3>Description</h3>
			<p class="lead">{!! $post->body !!}</p>
<!-- // carosel  photos-->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
  <!-- @foreach($images as $image) -->
  <!-- {{$image->filename}} -->

<div class="item">
	<img src="#" alt="{{$image->filename}}"  style="width:100%;">
</div>
  <!-- @endforeach -->
  </div>

  <!-- Left and right controls -->
  <!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a> -->
</div>
<!-- // end caoroesl end -->

<!-- //  -->
<div class="w3-content w3-display-container" id="w3-content">
<!-- @foreach($images as $image)

	<img class="mySlides" src="https://upload.wikimedia.org/wikipedia/commons/4/41/Sunflower_from_Silesia2.jpg" alt="d"  style="width:100%;">
  @endforeach -->
  
  <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
  <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
</div>
<!-- //  -->
			<hr>
			@include('posts.templates.questionsTemplate')
			
			<hr>
			@if(Auth::User()->role_id== 1)
			<!-- post Quote -->
			<div class="row">
				<div class="col-md-12">
				<h1>Post Qoute</h1>
			@include('posts.templates.quoteTemplate')
				
				</div>
			</div>
			<!-- post Qoute End -->
			@endif
			<div class="tags">
				@foreach ($post->tags as $tag)
					<span class="label label-default">{{ $tag->name }}</span>
				@endforeach
			</div>
@if($post->status == 5)
	@if(Auth::user())
		@if($post->user_id == Auth::user()->id)
			<div class="row">
				<div id="comment-form" class="col-md-8 col-md-offset-2" style="margin-top: 50px;">
					{{ Form::open(['route' => ['comments.store', $post->id], 'method' => 'POST']) }}

						<div class="row">
						<input type="hidden" value="{{Auth::User()->id}}" name="id">
							<div class="col-md-6" hidden>
								{{ Form::label('name', "Name:") }}
								{{ Form::text('name', Auth::User()->name, ['class' => 'form-control']) }}
							</div>

							<div class="col-md-6" hidden>
								{{ Form::label('email', 'Email:') }}
								{{ Form::text('email', Auth::User()->email, ['class' => 'form-control']) }}
							</div>
							<div class="col-md-12">
								{{ Form::label('comment', "Post Review:") }}
								{{ Form::textarea('comment', null, ['class' => 'form-control', 'rows' => '5']) }}

								{{ Form::submit('Add Review', ['class' => 'btn btn-success btn-block', 'style' => 'margin-top:15px;']) }}
							</div>
						</div>

					{{ Form::close() }}
				</div>
			</div>
		@endif
	@endif
			<div id="backend-comments" style="margin-top: 50px;">
				<h3>Reviews <small>{{ $post->comments()->count() }} total</small></h3>

				<table class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>message</th>
							<th>Stars</th>
							@if(Auth::User()->role_id ==1)
							<th width="70px"></th>
							@endif
						</tr>
					</thead>

					<tbody>
						@foreach ($post->comments as $comment)
						<tr>
							<td>{{ $comment->name }}</td>
							<td>{{ $comment->email }}</td>
							<td>{{ $comment->comment }}</td>
							<td></td>
							@if(Auth::User()->role_id ==1)
							<td>
								<a href="{{ route('comments.edit', $comment->id) }}" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
								<a href="{{ route('comments.delete', $comment->id) }}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
							@endif
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
@endif
		</div>

		<div class="col-md-4">
			<div class="well">
				<dl class="dl-horizontal">
					<label>Url:</label>
					<p><a href="{{ route('blog.single', $post->slug) }}">{{ route('blog.single', $post->slug) }}</a></p>
				</dl>

				<dl class="dl-horizontal">
					<label>Category:</label>
					<p>{{ $post->category->name }}</p>
				</dl>

				<dl class="dl-horizontal">
					<label>Created At:</label>
					<p>{{ date('M j, Y h:ia', strtotime($post->created_at)) }}</p>
				</dl>

				<dl class="dl-horizontal">
					<label>Last Updated:</label>
					<p>{{ date('M j, Y h:ia', strtotime($post->updated_at)) }}</p>
				</dl>
				<dl class="dl-horizontal">
					<label>Status:</label>
					<p>
					@foreach($postStatus as $status)
						@if($status->id == $post->status)
							@if($post->status == 1)
							<button disabled="disabled" class="btn btn-warning btn-lg">{{$status->name}}</button>
							@elseif($post->status == 2)
							<button disabled="disabled" class="btn btn-success btn-lg">{{$status->name}}</button>
							@elseif($post->status == 3)
							<button disabled="disabled" class="btn btn-danger btn-lg">{{$status->name}}</button>
							@elseif($post->status == 4)
							<button disabled="disabled" class="btn btn-success btn-lg">{{$status->name}}</button>
							@else
							<button disabled="disabled" class="btn btn-success btn-lg">{{$status->name}}</button>
							@endif
						@endif
					@endforeach
					</p>
				</dl>
				<hr>
				@if(Auth::User()->role_id == 1)
				<div class="row">
					<div class="col-sm-12">
						{!! Html::linkRoute('posts.edit', 'Edit', array($post->id), array('class' => 'btn btn-primary btn-block btn-h1-spacing')) !!}
					</div>
				</div>
				@endif
				<div class="row">
					<div class="col-sm-12">
						{!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'DELETE']) !!}

						{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						{{ Html::linkRoute('posts.index', '<< See All Posts', array(), ['class' => 'btn btn-default btn-block btn-h1-spacing']) }}
					</div>
				</div>

			</div>
		</div>
	</div>

@endsection

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>

var postRoomsDimensions = [];
    @if($postRoomsDimensions_array)
    postRoomsDimensions = '{{json_encode($postRoomsDimensions_array)}}';
	postRoomsDimensions = JSON.parse(postRoomsDimensions.replace(/&quot;/g,'"'));  
	console.log(postRoomsDimensions);
    @else
// console.log("dd")
	@endif
	for(var x=0; x < postRoomsDimensions.length; x++){
        $('#room_dimentions').append(`
            <label for="validationCustom02">Room `+x+`: Details</label>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Length</label>
                        <input type="number" min=0 class="form-control" id="length" name="room_dimensions[length][]" value=`+postRoomsDimensions[x]['length']+` placeholder="Length" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Height</label>
                        <input type="number" min=0 class="form-control" id="height" name="room_dimensions[height][]" value=`+postRoomsDimensions[x]['height']+` placeholder="Height" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Width</label>
                        <input type="number" min=0 class="form-control" id="width" name="room_dimensions[width][]" value=`+postRoomsDimensions[x]['width']+` placeholder="Width" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Description</label>
                        <textarea type="text" class="form-control" rows="3" id="description" name="room_dimensions[description][]" placeholder="Description" required>`+postRoomsDimensions[x]['description']+`</textarea>
                    </div>
                </div>
            `);
    }


var title = "{{$post->title}};";
var postal_code = "{{$post->postal_code}};";
var address = "{{$post->address}};"

var q1=JSON.parse("{{ json_encode($post->q1) }}".replace(/&quot;/g,'"'));
var q2=JSON.parse("{{ json_encode($post->q2) }}".replace(/&quot;/g,'"'));
var q3=JSON.parse("{{ json_encode($post->q3) }}".replace(/&quot;/g,'"'));
var q4=JSON.parse("{{ json_encode($post->q4) }}".replace(/&quot;/g,'"'));
var q5=JSON.parse("{{ json_encode($post->q5) }}".replace(/&quot;/g,'"'));
var q6=JSON.parse("{{ json_encode($post->q6) }}".replace(/&quot;/g,'"'));
var q7=JSON.parse("{{ json_encode($post->q7) }}".replace(/&quot;/g,'"'));
var q8=JSON.parse("{{ json_encode($post->q8) }}".replace(/&quot;/g,'"'));
var q9=JSON.parse("{{ json_encode($post->q9) }}".replace(/&quot;/g,'"'));
var q10=JSON.parse("{{ json_encode($post->q10) }}".replace(/&quot;/g,'"'));
var q11=JSON.parse("{{ json_encode($post->q11) }}".replace(/&quot;/g,'"'));
var q12=JSON.parse("{{ json_encode($post->q12) }}".replace(/&quot;/g,'"'));
var q13=JSON.parse("{{ json_encode($post->q13) }}".replace(/&quot;/g,'"'));
// $( document ).ready(function() {
	var images ="{{$images}}";
	images = JSON.parse("{{ json_encode($images)}}".replace(/&quot;/g,'"'));
	console.log( images);
	var container = document.getElementById('w3-content');
	for (var i = 0, j = images.length; i < j; i++) {
		var img = document.createElement('img');
		img.src = window.location.protocol+'//'+window.location.hostname+'/images/'+images[i]['filename']; // img[i] refers to the current URL.
		img.alt =images[i]['filename'];
		$("img").addClass('mySlides');
		container.appendChild(img);
	}
	// /
var slideIndex = 1;

showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}
function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}


// });

// /
</script>
<script src="{{asset('/js/checkbox_values.js')}}"></script>
@endsection