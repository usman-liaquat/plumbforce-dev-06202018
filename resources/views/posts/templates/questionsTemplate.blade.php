<!-- external form start-->
				<div class="form-row">
					<div class="col-md-6 mb-3">
					{{ Form::label('job_title', 'Title:') }}
					{{ Form::text('job_title', $post ?  $post->title : '', ["class" => 'form-control input-lg']) }}
						<!-- <label for="job_title">Job Name</label>
						<input type="text" class="form-control" id="job_title" name="job_title" placeholder="Job Name" required> -->
					</div>
					<div class="col-md-6 mb-3">
					{{ Form::label('postal_code', 'Postal Code:') }}
					{{ Form::text('postal_code', $post ?  $post->postal_code : '', ["class" => 'form-control input-lg']) }}
						<!-- <label for="postal_code">Postal Code</label>
						<input type="text" class="form-control" id="postal_code" placeholder="Postal Code" name="postal_code" required> -->
						<div hidden>
						{{ Form::text('slug', $post ? $post->slug: str_random(30), ["class" => 'form-control input-lg']) }}
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
					{{ Form::label('job_details', "Job Details:", ['class' => 'form-spacing-top']) }}
					{{ Form::textarea('job_details', $post ? $post->body : '' , ['class' => 'form-control']) }}
						<!-- <label for="job_details">Job Details</label>
						<textarea type="text" rows="4" class="form-control" id="job_details" name="job_details" placeholder="Job Details"></textarea> -->
					</div>
					<div class="col-md-6 mb-3">
					{{ Form::label('job_address', "Job Address:", ['class' => 'form-spacing-top']) }}
					{{ Form::textarea('job_address', $post ? $post->address : '' , ['class' => 'form-control']) }}
						<!-- <label for="job_address">Job Address</label>
						<textarea type="text"  rows="4" class="form-control" id="job_address" name="job_address" placeholder="Job Details"></textarea> -->
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">WHAT KIND OF FUEL DOES YOUR BOILER USE?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="gas" name="q1" value="gas" required>
							<label class="custom-control-label" for="gas">Gas</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="electricity" value="electricity" name="q1" required>
							<label class="custom-control-label" for="electricity">Electricity</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
						<div class="custom-control custom-radio d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="lpg" name="q1" value="lpg" required>
							<label class="custom-control-label" for="lpg">LPG</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="oil" name="q1" value="oil" required>
							<label class="custom-control-label" for="oil">Oil</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">WHAT TYPE OF BOILER DO YOU HAVE?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="Combi" name="q2" value="Combi" required>
							<label class="custom-control-label" for="Combi">Combi</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="Standard" name="q2" value="Standard" required>
							<label class="custom-control-label" for="Standard">Standard</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
						<div class="custom-control custom-radio d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="System" name="q2" value="System" required>
							<label class="custom-control-label" for="System">System</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="Back_Boiler" name="q2" value="Back_Boiler" required>
							<label class="custom-control-label" for="Back Boiler">Back Boiler</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">DO YOU WANT YOUR NEW BOILER IN A DIFFERENT PLACE?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="yes" name="q3" value="yes" required>
							<label class="custom-control-label" for="yes">yes</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="no" name="q3" value="no" required>
							<label class="custom-control-label" for="no">no</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">WHICH OF THESE BEST DESCRIBES YOUR HOME?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="Detached" name="q4" value="Detached" required>
							<label class="custom-control-label" for="Detached">Detached</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="Semi-Detached" name="q4" value="Semi-Detached" required>
							<label class="custom-control-label" for="Semi-Detached">Semi-Detached</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
						<div class="custom-control custom-radio d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="Tarrace" name="q4" value="Tarrace" required>
							<label class="custom-control-label" for="Tarrace">Tarrace</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="Bangalow" name="q4" value="Bangalow" required>
							<label class="custom-control-label" for="Bangalow">Bangalow</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">HOW MANY BEDROOMS DO YOU HAVE?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="q5_1" name="q5" value="1" required>
							<label class="custom-control-label" for="q5_1">1</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q5_2" value="2"  name="q5" required>
							<label class="custom-control-label" for="q5_2">2</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
						<div class="custom-control custom-radio d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q5_3" value="3"  name="q5" required>
							<label class="custom-control-label" for="q5_3">3</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q5_4" value="4"  name="q5" required>
							<label class="custom-control-label" for="q5_4">4</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q5_5" value="5"  name="q5" required>
							<label class="custom-control-label" for="q5_5">5</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q5_6" value="6"  name="q5" required>
							<label class="custom-control-label" for="q5_6">6</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
					</div>
				</div>
				<div id="room_dimentions"></div>
				<div class="form-row">
					<label for="validationCustom02">HOW MANY BATHTUBS DO YOU HAVE, OR PLAN TO HAVE IN FUTURE?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="q6_0" name="q6" value="q6_0" required>
							<label class="custom-control-label" for="q6_0">0</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q6_1" name="q6" value="q6_1" required>
							<label class="custom-control-label" for="q6_1">1</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
						<div class="custom-control custom-radio d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q6_2" name="q6" value="q6_2" required>
							<label class="custom-control-label" for="q6_2">2</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q6_3" name="q6" value="q6_3" required>
							<label class="custom-control-label" for="q6_3">3 +</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">HOW MANY SEPARATE SHOWERS DO YOU HAVE, OR PLAN TO HAVE IN FUTURE?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="q7_0" name="q7" value="q7_0" required>
							<label class="custom-control-label" for="q7_0">0</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q7_1" name="q7" value="q7_1" required>
							<label class="custom-control-label" for="q7_1">1</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
						<div class="custom-control custom-radio d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q7_2" name="q7" value="q7_2" required>
							<label class="custom-control-label" for="q7_2">2 +</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">DO YOU HAVE THERMOSTATIC RADIATOR VALVES?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="q8_1" name="q8" value="q8_1" required>
							<label class="custom-control-label" for="q8_1">yes</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q8_2" name="q8" value="q8_2" required>
							<label class="custom-control-label" for="q8_2">no</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">WHERE DOES YOUR FLUE COME OUT?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="roof" name="q9" value="roof" required>
							<label class="custom-control-label" for="roof">roof</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="wall" name="q9" value="wall" required>
							<label class="custom-control-label" for="wall">wall</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">IS YOUR FLUE ON A SLOPED ROOF OR A FLAT ROOF?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="Sloped" name="q10" value="Sloped" required>
							<label class="custom-control-label" for="Sloped">Sloped</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="Flat" name="q10" value="Flat" required>
							<label class="custom-control-label" for="Flat">Flat</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">WHERE ON THE ROOF IS IT POSITIONED?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="Highest_two_thirds" name="q11" value="Highest_two_thirds" required>
							<label class="custom-control-label" for="Highest two-thirds">Highest two-thirds</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="Lowest_third" name="q11" value="Lowest_third" required>
							<label class="custom-control-label" for="Lowest third">Lowest third</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">ARE YOU THE LANDLORD FOR THIS PROPERTY?</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="Yes" name="q12" value="Yes" required>
							<label class="custom-control-label" for="Yes">Yes</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="No" name="q12" value="No" required>
							<label class="custom-control-label" for="No">No</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
					</div>
				</div>
				<div class="form-row">
					<label for="validationCustom02">SELECT BOILER MODEL</label>
					<div class="col-md-12 mb-3">
						<div class="custom-control custom-radio d-inline-block mx-4 ">
							<input type="radio" class="custom-control-input" id="q13_1" name="q13" value="q13_1" required>
							<label class="custom-control-label" for=" q13_1"> Boiler e1 Price £2,100</label>
							<!-- <div class="invalid-feedback">Electricity</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q13_2" name="q13" value="q13_2" required>
							<label class="custom-control-label" for="q13_2">Greenstar 30Si Price £2,300</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
						<div class="custom-control custom-radio mb-3 d-inline-block mx-4">
							<input type="radio" class="custom-control-input" id="q13_3" name="q13" value="q13_3" required>
							<label class="custom-control-label" for="q13_3">Greenstar 25i Price £2,200</label>
							<!-- <div class="invalid-feedback">s</div> -->
						</div>
					</div>
				</div>
				<!-- external form end -->