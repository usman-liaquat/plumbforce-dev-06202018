<!DOCTYPE html>
<html lang="en">
  <head>
    @include('partials._head')
    <style>
    .page-heading {
    margin: 20px 0;
    color: #666;
    -webkit-font-smoothing: antialiased;
    font-family: "Segoe UI Light", "Arial", serif;
    font-weight: 600;
    letter-spacing: 0.05em;
}

#my-dropzone .message {
    font-family: "Segoe UI Light", "Arial", serif;
    font-weight: 600;
    color: #0087F7;
    font-size: 1.5em;
    letter-spacing: 0.05em;
}

.dropzone {
    border: 2px dashed #0087F7;
    background: white;
    border-radius: 5px;
    min-height: 300px;
    padding: 90px 0;
    vertical-align: baseline;
}
</style>
  </head>
  
  <body>

    @include('partials._nav')    

    <div class="container">
      @include('partials._messages')

      @yield('content')

      @include('partials._footer')

    </div> <!-- end of .container --> 
        @include('partials._javascript')
        <script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
$(document).ready(function(){
    var q5 = $('input[type=radio][name=q5]');
    // current projects data 
    

   
    // end current rooms diemensions
    q5.change(function() {
        $('#room_dimentions').empty();
        var room_count = $('input[name=q5]:checked').val();
        console.log(room_count);
        for(i = 1; i <= room_count ; i++) {
            $('#room_dimentions').append(`
            <label for="validationCustom02">Room `+i+`: Details</label>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Length</label>
                        <input type="number" min=0 class="form-control" id="length" name="room_dimensions[length][]" placeholder="Length" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Height</label>
                        <input type="number" min=0 class="form-control" id="height" name="room_dimensions[height][]" placeholder="Height" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Width</label>
                        <input type="number" min=0 class="form-control" id="width" name="room_dimensions[width][]" placeholder="Width" required>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom02">Description</label>
                        <textarea type="text" class="form-control" rows="3" id="description" name="room_dimensions[description][]" placeholder="Description" required></textarea>
                    </div>
                </div>
            `);
        }
    });
});

</script>
        @yield('scripts')

  </body>
</html>
