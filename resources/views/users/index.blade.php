@extends('main')

@section('title', '| All Users')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>All Users</h1>
		</div>

		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>Name</th>
					<th>Role</th>
					<th>Status</th>
					<th>Created At</th>
				</thead>

				<tbody>
					
					@foreach ($users as $user)
						
						<tr>
							<th>{{ $user->name }}</th>
							<td>{{ $user->user_role }}</td>
							<td>{{ $user->user_status }}</td>
							<td>{{ date('M j, Y', strtotime($user->created_at)) }}</td>
						</tr>

					@endforeach

				</tbody>
			</table>

			
		</div>
	</div>

@stop