<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserAccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@user.com',
            'role_id' => 1,
            'status' => 1,
            'password' => bcrypt('secret')
        ]); 
        DB::table('users')->insert([
            'name' => 'Moderator',
            'email' => 'moderator@user.com',
            'role_id' => 2,
            'status' => 1,
            'password' => bcrypt('secret')
        ]);
        DB::table('users')->insert([
            'name' => 'User',
            'email' => 'user@user.com',
            'role_id' => 3,
            'status' => 1,
            'password' => bcrypt('secret')
        ]);
    }
}
