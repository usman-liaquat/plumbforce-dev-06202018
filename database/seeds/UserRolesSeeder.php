<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            'name' => 'admin',
        ]); 
        DB::table('user_roles')->insert([
            'name' => 'moderator',
        ]);
        DB::table('user_roles')->insert([
            'name' => 'user',
        ]);
    }
}
