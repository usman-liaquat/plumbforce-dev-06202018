<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserRolesSeeder::class);
        $this->call(PostStatusSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(UserAccountsSeeder::class);
        $this->call(UserStatusSeeder::class);

    }
}
