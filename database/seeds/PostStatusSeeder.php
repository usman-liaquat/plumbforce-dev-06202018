<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_status')->insert([
            'name' => 'pending',
        ]); 
        DB::table('post_status')->insert([
            'name' => 'approved',
        ]);
        DB::table('post_status')->insert([
            'name' => 'rejected',
        ]);
        DB::table('post_status')->insert([
            'name' => 'OnGoing',
        ]);
        DB::table('post_status')->insert([
            'name' => 'completed',
        ]);
    }
}
