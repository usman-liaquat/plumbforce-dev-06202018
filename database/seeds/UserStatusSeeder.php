<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_status')->insert([
            'name' => 'approved',
        ]); 
        DB::table('user_status')->insert([
            'name' => 'pending',
        ]);
        DB::table('user_status')->insert([
            'name' => 'rejected',
        ]);
    }
}
