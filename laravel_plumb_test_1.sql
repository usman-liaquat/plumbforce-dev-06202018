-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2018 at 10:32 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_plumb_test_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Boiler', NULL, NULL),
(2, 'Heating', NULL, NULL),
(3, 'Water Heater', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `name`, `email`, `comment`, `approved`, `post_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 2, 'usman user', 'usman@user.com', 'helllo hi test review', 1, 4, NULL, '2018-06-15 03:41:27', '2018-06-15 03:41:27'),
(3, 2, 'usman user', 'usman@user.com', 'quite satistfied with services ', 1, 4, NULL, '2018-06-15 03:42:20', '2018-06-15 03:42:20');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_02_06_175142_create_posts_table', 1),
('2016_03_20_162017_add_slug_to_users', 1),
('2016_04_28_021908_create_categories_table', 1),
('2016_04_28_022255_add_category_id_to_posts', 1),
('2016_05_30_153615_create_tags_table', 1),
('2016_05_30_155417_create_post_tag_table', 1),
('2016_07_16_173641_create_comments_table', 1),
('2016_08_15_000718_add_image_col_to_posts', 1),
('2018_06_15_003105_post_type_table', 1),
('2018_06_15_003954_user_roles_table', 1),
('2018_06_15_092913_create_post_status_table', 2),
('2018_06_17_100040_create_room_dimentions_table', 3),
('2018_06_17_101152_create_post_media_file_table', 4),
('2018_06_18_163510_create_room_dimensions_table', 5),
('2018_06_18_163645_post_media_files_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q10` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q11` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q12` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `q13` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `status`, `title`, `q1`, `q2`, `q3`, `q4`, `q5`, `q6`, `q7`, `q8`, `q9`, `q10`, `q11`, `q12`, `q13`, `postal_code`, `body`, `slug`, `image`, `category_id`, `address`, `deleted_at`, `created_at`, `updated_at`) VALUES
(4, 2, '1', 'Cumque cupidatat exercitationem perferendis rerum eum dolores in sunt cillum minima doloremque porro', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '<p>dadada ad ajgdw ajd</p>', '1222111', '1529051214.jpg', 1, '', NULL, '2018-06-15 03:26:55', '2018-06-15 03:28:00'),
(5, 2, '1', 'Sed quisquam officia aute qui voluptate consequat Non itaque aliquip sunt', 'gas', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '12322', NULL, 2, '', NULL, '2018-06-17 03:57:28', '2018-06-17 03:57:28'),
(6, 2, '1', 'Dolor quisquam sunt architecto est et ut sit ut mollit sit culpa tenetur eum dignissimos in est', 'oil', 'System', 'yes', 'Detached', '1', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', '', '', '78', NULL, 3, '', NULL, '2018-06-17 04:37:06', '2018-06-17 04:37:06'),
(7, 2, '1', 'Bevis Mccullough', 'electricity', 'Back_Boiler', 'yes', 'Tarrace', '5', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'Id voluptas ut commodi sint enim', '', '67', NULL, 3, '', NULL, '2018-06-17 04:43:52', '2018-06-17 04:43:52'),
(8, 2, '1', 'Alfonso Sears', 'electricity', 'Combi', 'yes', 'Detached', '4', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'Consequatur dolore incididunt omnis labore quasi dolorem et quasi inventore aspernatur totam eiusmod', '<p>detials</p>', '70', NULL, 2, '<p>dkjhaw kdhajkdka djkh</p>', NULL, '2018-06-17 04:45:22', '2018-06-17 04:45:22'),
(9, 2, '1', 'Preston Vasquez', 'gas', 'Standard', 'yes', 'Detached', '2', 'q6_3', 'q7_2', 'q8_1', 'wall', 'Sloped', 'Highest_two_thirds', 'Yes', 'q13_1', 'Vel autem illum voluptatem quis earum itaque et in neque architecto deserunt consectetur enim temporibus ipsam eum vitae', '<p>details jobs </p>', '68', NULL, 3, '<p>job <strong>address</strong></p>', NULL, '2018-06-17 04:57:06', '2018-06-17 04:57:06'),
(10, 2, '1', 'Justina Henson', 'oil', 'System', 'no', 'Detached', '5', 'q6_0', 'q7_2', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_1', 'Id similique quam ducimus autem quae voluptas sint in sunt vero est', '', '72', NULL, 2, '', NULL, '2018-06-17 07:41:58', '2018-06-17 07:41:58'),
(11, 2, '1', 'Justina Henson', 'oil', 'System', 'no', 'Detached', '5', 'q6_0', 'q7_2', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_1', 'Id similique quam ducimus autem quae voluptas sint in sunt vero est', '', '91', NULL, 2, '', NULL, '2018-06-17 07:48:09', '2018-06-17 07:48:09'),
(12, 2, '1', 'Justina Henson', 'oil', 'System', 'no', 'Detached', '5', 'q6_0', 'q7_2', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_1', 'Id similique quam ducimus autem quae voluptas sint in sunt vero est', '', '23', NULL, 2, '', NULL, '2018-06-17 07:49:06', '2018-06-17 07:49:06'),
(13, 2, '1', 'Justina Henson', 'oil', 'System', 'no', 'Detached', '5', 'q6_0', 'q7_2', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_1', 'Id similique quam ducimus autem quae voluptas sint in sunt vero est', '', '36', NULL, 2, '', NULL, '2018-06-17 07:50:08', '2018-06-17 07:50:08'),
(14, 2, '1', 'Justina Henson', 'oil', 'System', 'no', 'Detached', '5', 'q6_0', 'q7_2', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_1', 'Id similique quam ducimus autem quae voluptas sint in sunt vero est', '', '46', NULL, 2, '', NULL, '2018-06-17 07:51:37', '2018-06-17 07:51:37'),
(15, 2, '1', 'Justina Henson', 'oil', 'System', 'no', 'Detached', '5', 'q6_0', 'q7_2', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_1', 'Id similique quam ducimus autem quae voluptas sint in sunt vero est', '', '55', NULL, 2, '', NULL, '2018-06-17 07:52:57', '2018-06-17 07:52:57'),
(16, 2, '1', 'Beck Hardin', 'oil', 'Combi', 'no', 'Bangalow', '5', 'q6_3', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Minima illum at et temporibus totam minima quidem vitae voluptas doloribus tempora ad est', '', '73', NULL, 2, '', NULL, '2018-06-18 23:39:36', '2018-06-18 23:39:36'),
(17, 2, '1', 'Beck Hardin', 'oil', 'Combi', 'no', 'Bangalow', '5', 'q6_3', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Minima illum at et temporibus totam minima quidem vitae voluptas doloribus tempora ad est', '', '26', NULL, 2, '', NULL, '2018-06-18 23:41:39', '2018-06-18 23:41:39'),
(18, 2, '1', 'Beck Hardin', 'oil', 'Combi', 'no', 'Bangalow', '5', 'q6_3', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Minima illum at et temporibus totam minima quidem vitae voluptas doloribus tempora ad est', '', '87', NULL, 2, '', NULL, '2018-06-18 23:43:14', '2018-06-18 23:43:14'),
(19, 2, '1', 'Beck Hardin', 'oil', 'Combi', 'no', 'Bangalow', '5', 'q6_3', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Minima illum at et temporibus totam minima quidem vitae voluptas doloribus tempora ad est', '', '63', NULL, 2, '', NULL, '2018-06-18 23:45:13', '2018-06-18 23:45:13'),
(21, 2, '1', 'Beck Hardin', 'oil', 'Combi', 'no', 'Bangalow', '5', 'q6_3', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Minima illum at et temporibus totam minima quidem vitae voluptas doloribus tempora ad est', '', '49', NULL, 2, '', NULL, '2018-06-18 23:47:08', '2018-06-18 23:47:08'),
(22, 2, '1', 'Beck Hardin', 'oil', 'Combi', 'no', 'Bangalow', '5', 'q6_3', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Minima illum at et temporibus totam minima quidem vitae voluptas doloribus tempora ad est', '', '29', NULL, 2, '', NULL, '2018-06-18 23:48:31', '2018-06-18 23:48:31'),
(23, 2, '1', 'Beck Hardin', 'oil', 'Combi', 'no', 'Bangalow', '5', 'q6_3', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Minima illum at et temporibus totam minima quidem vitae voluptas doloribus tempora ad est', '', '88', NULL, 2, '', NULL, '2018-06-18 23:48:40', '2018-06-18 23:48:40'),
(24, 2, '1', 'Beck Hardin', 'oil', 'Combi', 'no', 'Bangalow', '5', 'q6_3', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Minima illum at et temporibus totam minima quidem vitae voluptas doloribus tempora ad est', '', '20', NULL, 2, '', NULL, '2018-06-18 23:52:39', '2018-06-18 23:52:39'),
(25, 2, '1', 'Beck Hardin', 'oil', 'Combi', 'no', 'Bangalow', '5', 'q6_3', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Minima illum at et temporibus totam minima quidem vitae voluptas doloribus tempora ad est', '', '92', NULL, 2, '', NULL, '2018-06-18 23:55:28', '2018-06-18 23:55:28'),
(26, 2, '1', 'Beck Hardin', 'oil', 'Combi', 'no', 'Bangalow', '5', 'q6_3', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Minima illum at et temporibus totam minima quidem vitae voluptas doloribus tempora ad est', '', '22', NULL, 2, '', NULL, '2018-06-18 23:56:08', '2018-06-18 23:56:08'),
(27, 2, '1', 'Beck Hardin', 'oil', 'Combi', 'no', 'Bangalow', '5', 'q6_3', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Minima illum at et temporibus totam minima quidem vitae voluptas doloribus tempora ad est', '', '71', NULL, 2, '', NULL, '2018-06-18 23:56:29', '2018-06-18 23:56:29'),
(28, 2, '1', 'Richard Moss', 'lpg', 'Back_Boiler', 'no', 'Detached', '2', 'q6_0', 'q7_1', 'q8_2', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Error dolor dolor voluptatem et eaque dolore dolorem mollitia in natus voluptatem ea', '', '84', NULL, 2, '', NULL, '2018-06-18 23:58:03', '2018-06-18 23:58:03'),
(29, 2, '1', 'Rama Stephens', 'oil', 'Back_Boiler', 'yes', 'Tarrace', '6', 'q6_1', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_1', 'Est doloremque sed natus in', '', '33', NULL, 3, '', NULL, '2018-06-18 23:58:54', '2018-06-18 23:58:54'),
(31, 2, '1', 'Fritz Justice', 'gas', 'Combi', 'yes', 'Detached', '5', 'q6_0', 'q7_1', 'q8_2', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_3', 'Molestias enim voluptatem qui quo veritatis ut ad incididunt eaque dolores', '', '21', NULL, 3, '', NULL, '2018-06-19 02:29:45', '2018-06-19 02:29:45'),
(32, 2, '1', 'Brady Bonner', 'electricity', 'Back_Boiler', 'no', 'Bangalow', '3', 'q6_2', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Eaque cupidatat doloribus velit dicta aliqua Quis consequatur Fuga Voluptatem Pariatur Voluptas', '', '32', NULL, 2, '', NULL, '2018-06-19 02:30:46', '2018-06-19 02:30:46'),
(33, 2, '1', 'Brady Bonner', 'electricity', 'Back_Boiler', 'no', 'Bangalow', '3', 'q6_2', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Eaque cupidatat doloribus velit dicta aliqua Quis consequatur Fuga Voluptatem Pariatur Voluptas', '', '66', NULL, 2, '', NULL, '2018-06-19 02:31:15', '2018-06-19 02:31:15'),
(34, 2, '1', 'Brady Bonner', 'electricity', 'Back_Boiler', 'no', 'Bangalow', '3', 'q6_2', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Eaque cupidatat doloribus velit dicta aliqua Quis consequatur Fuga Voluptatem Pariatur Voluptas', '', '48', NULL, 2, '', NULL, '2018-06-19 02:32:53', '2018-06-19 02:32:53'),
(35, 2, '1', 'Brady Bonner', 'electricity', 'Back_Boiler', 'no', 'Bangalow', '3', 'q6_2', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Eaque cupidatat doloribus velit dicta aliqua Quis consequatur Fuga Voluptatem Pariatur Voluptas', '', '40', NULL, 2, '', NULL, '2018-06-19 02:32:57', '2018-06-19 02:32:57'),
(36, 2, '1', 'Brady Bonner', 'electricity', 'Back_Boiler', 'no', 'Bangalow', '3', 'q6_2', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Eaque cupidatat doloribus velit dicta aliqua Quis consequatur Fuga Voluptatem Pariatur Voluptas', '', '15', NULL, 2, '', NULL, '2018-06-19 02:33:13', '2018-06-19 02:33:13'),
(37, 2, '1', 'Brady Bonner', 'electricity', 'Back_Boiler', 'no', 'Bangalow', '3', 'q6_2', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Eaque cupidatat doloribus velit dicta aliqua Quis consequatur Fuga Voluptatem Pariatur Voluptas', '', '90', NULL, 2, '', NULL, '2018-06-19 02:34:05', '2018-06-19 02:34:05'),
(38, 2, '1', 'Brady Bonner', 'electricity', 'Back_Boiler', 'no', 'Bangalow', '3', 'q6_2', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Eaque cupidatat doloribus velit dicta aliqua Quis consequatur Fuga Voluptatem Pariatur Voluptas', '', '24', NULL, 2, '', NULL, '2018-06-19 02:34:32', '2018-06-19 02:34:32'),
(40, 2, '1', 'Brady Bonner', 'electricity', 'Back_Boiler', 'no', 'Bangalow', '3', 'q6_2', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Eaque cupidatat doloribus velit dicta aliqua Quis consequatur Fuga Voluptatem Pariatur Voluptas', '', '69', NULL, 2, '', NULL, '2018-06-19 02:34:48', '2018-06-19 02:34:48'),
(42, 2, '1', 'Brady Bonner', 'electricity', 'Back_Boiler', 'no', 'Bangalow', '3', 'q6_2', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Eaque cupidatat doloribus velit dicta aliqua Quis consequatur Fuga Voluptatem Pariatur Voluptas', '', '93', NULL, 2, '', NULL, '2018-06-19 02:34:52', '2018-06-19 02:34:52'),
(47, 2, '1', 'Francis Perez', 'gas', 'Combi', 'yes', 'Tarrace', '2', 'q6_3', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_3', 'Est debitis id quia voluptatum sed dolorum provident minim asperiores nesciunt eligendi in aut ut vel', '', '28', NULL, 2, '', NULL, '2018-06-19 02:35:47', '2018-06-19 02:35:47'),
(48, 2, '1', 'Francis Perez', 'gas', 'Combi', 'yes', 'Tarrace', '2', 'q6_3', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_3', 'Est debitis id quia voluptatum sed dolorum provident minim asperiores nesciunt eligendi in aut ut vel', '', '74', NULL, 2, '', NULL, '2018-06-19 02:36:32', '2018-06-19 02:36:32'),
(49, 2, '1', 'Shellie Hancock', 'electricity', 'System', 'yes', 'Bangalow', '6', 'q6_3', 'q7_0', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_2', 'Et magnam dolor illo molestiae', '', '54', NULL, 3, '', NULL, '2018-06-19 02:37:31', '2018-06-19 02:37:31'),
(50, 2, '1', 'Lacota Collins', 'electricity', 'Standard', 'no', 'Bangalow', '2', 'q6_2', 'q7_0', 'q8_1', 'roof', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_1', 'Tempor nisi ullam quam officiis cumque omnis odit rerum ea cum temporibus perferendis nihil dolore eos in pariatur Eius', '', '43', NULL, 3, '', NULL, '2018-06-19 02:39:17', '2018-06-19 02:39:17'),
(51, 2, '1', 'Brooke Bradley', 'gas', 'Back_Boiler', 'no', 'Semi-Detached', '3', 'q6_3', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_3', 'Velit ut voluptatibus reiciendis facilis eum illo repudiandae cupiditate enim commodo', '', '89', NULL, 2, '', NULL, '2018-06-19 02:53:12', '2018-06-19 02:53:12'),
(54, 2, '1', 'Maris Scott;', 'electricity', 'Combi', 'no', 'Detached', '2', 'q6_1', 'q7_0', 'q8_2', 'wall', 'Sloped', 'Highest_two_thirds', 'No', 'q13_2', 'Aliquam consequatur eligendi lorem dolores corporis ullam sit velit proident duis ab quia reprehenderit nihil quis', 'hello dwajdh akdhak daw', '80', NULL, 3, 'add ress 54 body da dtttttttttttttttttt', NULL, '2018-06-19 02:56:21', '2018-06-20 14:48:42'),
(58, 2, '1', 'Maris Scott', 'electricity', 'Combi', 'no', 'Detached', '1', 'q6_1', 'q7_0', 'q8_2', 'wall', 'Sloped', 'Highest_two_thirds', 'No', 'q13_2', 'Aliquam consequatur eligendi lorem dolores corporis ullam sit velit proident duis ab quia reprehenderit nihil quis', '', '96', NULL, 3, '', NULL, '2018-06-19 02:56:36', '2018-06-19 02:56:36'),
(59, 2, '2', 'Jesse Tanner', 'electricity', 'Back_Boiler', 'yes', 'Semi-Detached', '6', 'q6_3', 'q7_0', 'q8_1', 'wall', 'Flat', 'Lowest_third', 'No', 'q13_2', 'Excepteur consequat Officia sint enim iusto ad mollitia sint neque sequi ipsum tenetur blanditiis voluptates consectetur reprehenderit laboriosam', '<p>kilo</p>', 'HGertYznsvUZzzGzt8778fu2qQSQRLa', NULL, 2, '', NULL, '2018-06-19 02:57:27', '2018-06-19 17:25:19'),
(61, 2, '1', 'Odio pariatur Est consectetur laudantium in rem quae et recusandae Debitis illo architecto Nam voluptas commodi inventore minus voluptas', 'electricity', 'System', 'yes', 'Bangalow', '1', 'q6_0', 'q7_0', 'q8_2', 'roof', 'Sloped', 'Lowest_third', 'Yes', 'q13_3', 'Aut laboris aut fugit ab ex soluta hic voluptate aliquid mollitia aut enim in qui porro velit', '<p>dawdaw&nbsp;</p>', 'VK0QfacZkQRcuxQRHaIMoc9QIyKZy4', NULL, 3, '<p>sdwadaw</p>', NULL, '2018-06-21 01:02:12', '2018-06-21 01:02:12'),
(62, 2, '1', 'Nostrum in exercitationem voluptate fugiat eligendi', 'oil', 'Back_Boiler', 'no', 'Tarrace', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Sloped', 'Lowest_third', 'Yes', 'q13_1', 'Voluptas quod necessitatibus molestiae tempore recusandae Dolor ea pariatur Blanditiis veniam aliqua', '<p>hello deyaisl&nbsp;</p>', 'vZkdInAXeThB8AI7Rq7UzIN2eppfd8', NULL, 3, '<p>hello address</p>', NULL, '2018-06-21 12:09:20', '2018-06-21 12:09:20'),
(63, 2, '1', 'Soluta earum in nisi quo maxime laboriosam in alias est voluptates atque pariatur Non voluptatem consequatur ut aut quasi', 'lpg', 'System', 'yes', 'Bangalow', '1', 'q6_1', 'q7_2', 'q8_1', 'roof', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_3', 'Sint non quia voluptas magnam atque sunt exercitation sed corrupti', '<p>helll dedatdy a d</p>', 'RSdxkYBtV8xfTrAzLWGpcGLRV8f1Dj', NULL, 3, '<p>&nbsp;adadddredss sd</p>', NULL, '2018-06-21 12:12:21', '2018-06-21 12:12:21'),
(64, 2, '1', 'Impedit accusantium reprehenderit laborum Eos et sunt laboriosam dolor alias voluptas molestiae voluptates ratione facere nesciunt', 'electricity', 'Back_Boiler', 'yes', 'Detached', '1', 'q6_3', 'q7_0', 'q8_1', 'wall', 'Sloped', 'Lowest_third', 'Yes', 'q13_2', 'Est quo obcaecati voluptatem voluptatum', '<p>dawda dawjd awwdawkj dd&nbsp;</p>', 'o93rzFL160NVGc82CKzmFvAA6KGWJu', NULL, 3, '<p>&nbsp;dajwjdjakhdjk a</p>', NULL, '2018-06-21 12:44:05', '2018-06-21 12:44:05'),
(65, 2, '1', 'Cum animi velit vel a porro dolor vel consequatur eum ea quidem qui dolores lorem autem veniam facilis', 'oil', 'Combi', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Amet accusamus facilis in tenetur rerum', '<p>li dakjl alkwjlkawjdlk alkdjlk</p>', 'rvhFmuh5F8bptnFiESuGogmg9KNqtl', NULL, 3, '<p>jlk jklj</p>', NULL, '2018-06-21 12:44:38', '2018-06-21 12:44:38'),
(66, 2, '1', 'Consequatur ad eos nulla explicabo Dolore pariatur Expedita voluptatem duis magnam animi sint magnam autem sequi autem', 'electricity', 'Back_Boiler', 'no', 'Semi-Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_3', 'Ad vitae neque omnis labore expedita sed dolore dolor quidem', '<p>dja hdakjwh</p>', '2ZnAGWEZWfuCUIu4QWjbL1jGqRmr7y', NULL, 3, '<p>h jkhkjhkj</p>', NULL, '2018-06-21 14:32:45', '2018-06-21 14:32:45'),
(67, 2, '1', 'Consequatur ad eos nulla explicabo Dolore pariatur Expedita voluptatem duis magnam animi sint magnam autem sequi autem', 'electricity', 'Back_Boiler', 'no', 'Semi-Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_3', 'Ad vitae neque omnis labore expedita sed dolore dolor quidem', '<p>dja hdakjwh</p>', '2pj6P03jJV8hlmQ6KIzMTxVBGTrtL6', NULL, 3, '<p>h jkhkjhkj</p>', NULL, '2018-06-21 14:33:58', '2018-06-21 14:33:58'),
(68, 2, '1', 'Velit commodi qui doloremque ipsum vel sint et', 'gas', 'System', 'yes', 'Bangalow', '1', 'q6_2', 'q7_2', 'q8_1', 'roof', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_2', 'Veniam molestias aut voluptatibus obcaecati quia maxime similique', '<p>dadnawjkd k</p>', 'Z0epLgpPiGqAzWYC4YGjXte8TXm06n', NULL, 3, '<p>khkjh</p>', NULL, '2018-06-21 14:36:25', '2018-06-21 14:36:25'),
(69, 2, '1', 'Enim eu eiusmod ut enim iure suscipit animi culpa', 'lpg', 'Standard', 'yes', 'Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Aperiam in laudantium enim cumque sunt incididunt incididunt', '<p>dada</p>', 'eOI0si5vJYVIUutYzLFWDNMYQGTjnB', NULL, 2, '<p>&nbsp;dadkjadkawkjd</p>', NULL, '2018-06-21 14:38:44', '2018-06-21 14:38:44'),
(70, 2, '1', 'Officia distinctio Provident eu id rerum libero ea non aut consequatur autem amet dolorem nulla totam iusto corrupti', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Temporibus harum ex voluptate quas quo ducimus eaque quod unde molestias labore sit aut', '<p>dad</p>', '7LXIZqzmU1YPHVA9JljVJKwFcyYRBk', NULL, 3, '<p>dadawda</p>', NULL, '2018-06-21 15:51:16', '2018-06-21 15:51:16'),
(71, 2, '1', 'Officia distinctio Provident eu id rerum libero ea non aut consequatur autem amet dolorem nulla totam iusto corrupti', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Temporibus harum ex voluptate quas quo ducimus eaque quod unde molestias labore sit aut', '<p>dad</p>', 'ZCamC6917w07bwcY2pDOei2Cpncolu', NULL, 3, '<p>dadawda</p>', NULL, '2018-06-21 15:51:58', '2018-06-21 15:51:58'),
(72, 2, '1', 'Officia distinctio Provident eu id rerum libero ea non aut consequatur autem amet dolorem nulla totam iusto corrupti', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Temporibus harum ex voluptate quas quo ducimus eaque quod unde molestias labore sit aut', '<p>dad</p>', 'Lvoh2CKE4MHa5qvpdXmGTYjpmQZMBW', NULL, 3, '<p>dadawda</p>', NULL, '2018-06-21 15:52:01', '2018-06-21 15:52:01'),
(73, 2, '1', 'Officia distinctio Provident eu id rerum libero ea non aut consequatur autem amet dolorem nulla totam iusto corrupti', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Temporibus harum ex voluptate quas quo ducimus eaque quod unde molestias labore sit aut', '<p>dad</p>', 'gNcP26ysdSk3jnARWz5r9feAdll7I3', NULL, 3, '<p>dadawda</p>', NULL, '2018-06-21 15:52:18', '2018-06-21 15:52:18'),
(74, 2, '1', 'Officia distinctio Provident eu id rerum libero ea non aut consequatur autem amet dolorem nulla totam iusto corrupti', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Temporibus harum ex voluptate quas quo ducimus eaque quod unde molestias labore sit aut', '<p>dad</p>', '181FjpQKpPnzPQ5VqviVarOS520q3I', NULL, 3, '<p>dadawda</p>', NULL, '2018-06-21 15:55:15', '2018-06-21 15:55:15'),
(75, 2, '1', 'Officia distinctio Provident eu id rerum libero ea non aut consequatur autem amet dolorem nulla totam iusto corrupti', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Temporibus harum ex voluptate quas quo ducimus eaque quod unde molestias labore sit aut', '<p>dad</p>', '7omNekVo0xvsFcvRNIxErgJW1z1KFo', NULL, 3, '<p>dadawda</p>', NULL, '2018-06-21 15:55:22', '2018-06-21 15:55:22'),
(76, 2, '1', 'Officia distinctio Provident eu id rerum libero ea non aut consequatur autem amet dolorem nulla totam iusto corrupti', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Temporibus harum ex voluptate quas quo ducimus eaque quod unde molestias labore sit aut', '<p>dad</p>', 'cR7jI8veNuUabCHLwvc3TFWnjh91pj', NULL, 3, '<p>dadawda</p>', NULL, '2018-06-21 15:57:40', '2018-06-21 15:57:40'),
(77, 2, '1', 'Officia distinctio Provident eu id rerum libero ea non aut consequatur autem amet dolorem nulla totam iusto corrupti', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Temporibus harum ex voluptate quas quo ducimus eaque quod unde molestias labore sit aut', '<p>dad</p>', 'e4rbEfLIhFDh2wCxQuRfi6LVoJ4YFI', NULL, 3, '<p>dadawda</p>', NULL, '2018-06-21 15:57:42', '2018-06-21 15:57:42'),
(78, 2, '1', 'Officia distinctio Provident eu id rerum libero ea non aut consequatur autem amet dolorem nulla totam iusto corrupti', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Temporibus harum ex voluptate quas quo ducimus eaque quod unde molestias labore sit aut', '<p>dad</p>', 'CO2KC40p8jdbQ6qgnCvBGlohiRBNEy', NULL, 3, '<p>dadawda</p>', NULL, '2018-06-21 15:57:58', '2018-06-21 15:57:58'),
(79, 2, '1', 'Officia distinctio Provident eu id rerum libero ea non aut consequatur autem amet dolorem nulla totam iusto corrupti', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Temporibus harum ex voluptate quas quo ducimus eaque quod unde molestias labore sit aut', '<p>dad</p>', 'qV2ScOclM2iY70c9FufvhOGgOgiS9g', NULL, 3, '<p>dadawda</p>', NULL, '2018-06-21 15:58:07', '2018-06-21 15:58:07'),
(80, 2, '1', 'Officia distinctio Provident eu id rerum libero ea non aut consequatur autem amet dolorem nulla totam iusto corrupti', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Temporibus harum ex voluptate quas quo ducimus eaque quod unde molestias labore sit aut', '<p>dad</p>', 'mIYWyqa1jAwtLAwTxHNNQhHRdH0vOc', NULL, 3, '<p>dadawda</p>', NULL, '2018-06-21 15:58:34', '2018-06-21 15:58:34'),
(81, 2, '1', 'Amet modi quidem ut cum qui labore tempore nemo adipisicing', 'electricity', 'System', 'yes', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Quibusdam voluptates aliquam beatae sed necessitatibus amet sunt assumenda fugiat quasi proident in in ut alias quia', '<p>dwad</p>', 'AYJvXqMUqAFtEUQ8j4MAuhC758a14I', NULL, 2, '<p>dadawd</p>', NULL, '2018-06-21 15:59:06', '2018-06-21 15:59:06'),
(82, 2, '1', 'Amet modi quidem ut cum qui labore tempore nemo adipisicing', 'electricity', 'System', 'yes', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Quibusdam voluptates aliquam beatae sed necessitatibus amet sunt assumenda fugiat quasi proident in in ut alias quia', '<p>dwad</p>', 'JIASbZmWdeIlbi2kQxRvOtJqivlaRP', NULL, 2, '<p>dadawd</p>', NULL, '2018-06-21 15:59:29', '2018-06-21 15:59:29'),
(83, 2, '1', 'Amet modi quidem ut cum qui labore tempore nemo adipisicing', 'electricity', 'System', 'yes', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Quibusdam voluptates aliquam beatae sed necessitatibus amet sunt assumenda fugiat quasi proident in in ut alias quia', '<p>dwad</p>', '9cO6mOC3FOof13y5wn8gJ01ePRrcpf', NULL, 2, '<p>dadawd</p>', NULL, '2018-06-21 16:00:06', '2018-06-21 16:00:06'),
(84, 2, '1', 'Amet modi quidem ut cum qui labore tempore nemo adipisicing', 'electricity', 'System', 'yes', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Quibusdam voluptates aliquam beatae sed necessitatibus amet sunt assumenda fugiat quasi proident in in ut alias quia', '<p>dwad</p>', 'iymQyd5C3QOV0uOSouRxwHUHiDGVZL', NULL, 2, '<p>dadawd</p>', NULL, '2018-06-21 16:00:22', '2018-06-21 16:00:22'),
(85, 2, '1', 'Amet modi quidem ut cum qui labore tempore nemo adipisicing', 'electricity', 'System', 'yes', 'Semi-Detached', '1', 'q6_0', 'q7_1', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Quibusdam voluptates aliquam beatae sed necessitatibus amet sunt assumenda fugiat quasi proident in in ut alias quia', '<p>dwad</p>', 'lHh9Eu1VP9ajifjr0vI6E66Ah9swNJ', NULL, 2, '<p>dadawd</p>', NULL, '2018-06-21 16:00:31', '2018-06-21 16:00:31'),
(86, 2, '1', 'Sit delectus id sint architecto at deserunt in facere rem repellendus Officiis dolor quos tempore ut ea quia quae iure', 'electricity', 'Back_Boiler', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Cumque fuga Consectetur quasi deserunt itaque labore quisquam et sit numquam culpa qui excepturi', '<p>dawd</p>', 'dkNbPCdtZkzN9YiyBBmLVzgWruaBz9', NULL, 3, '<p>addawdaw</p>', NULL, '2018-06-21 16:04:07', '2018-06-21 16:04:07'),
(87, 2, '1', 'Sit delectus id sint architecto at deserunt in facere rem repellendus Officiis dolor quos tempore ut ea quia quae iure', 'electricity', 'Back_Boiler', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Cumque fuga Consectetur quasi deserunt itaque labore quisquam et sit numquam culpa qui excepturi', '<p>dawd</p>', 'Vt6pBUAPfm0irbuxA6FeiISo2PR1IW', NULL, 3, '<p>addawdaw</p>', NULL, '2018-06-21 16:05:05', '2018-06-21 16:05:05'),
(88, 2, '1', 'Sit delectus id sint architecto at deserunt in facere rem repellendus Officiis dolor quos tempore ut ea quia quae iure', 'electricity', 'Back_Boiler', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_0', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_1', 'Cumque fuga Consectetur quasi deserunt itaque labore quisquam et sit numquam culpa qui excepturi', '<p>dawd</p>', 'guOK6haYwpxXaBA9Jq3gg6EPGtvahE', NULL, 3, '<p>addawdaw</p>', NULL, '2018-06-21 16:05:27', '2018-06-21 16:05:27'),
(89, 2, '1', 'Aut dignissimos duis maiores nulla officiis eveniet libero officiis magnam', 'oil', 'Back_Boiler', 'no', 'Tarrace', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_3', 'Laborum eius consequuntur ea qui voluptas sit culpa placeat quos reprehenderit deleniti tenetur perferendis sint suscipit quis adipisicing qui', '<p>dad</p>', 'EyvJmnjqFUjzZFlVzsnDxwNUANYpAP', NULL, 3, '<p>adda</p>', NULL, '2018-06-21 16:06:18', '2018-06-21 16:06:18'),
(90, 2, '1', 'Aut dignissimos duis maiores nulla officiis eveniet libero officiis magnam', 'oil', 'Back_Boiler', 'no', 'Tarrace', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_3', 'Laborum eius consequuntur ea qui voluptas sit culpa placeat quos reprehenderit deleniti tenetur perferendis sint suscipit quis adipisicing qui', '<p>dad</p>', 'rcP0ZsYBshwcDsbNTBpSywDtHPFj7n', NULL, 3, '<p>adda</p>', NULL, '2018-06-21 16:10:53', '2018-06-21 16:10:53'),
(91, 2, '1', 'Aut dignissimos duis maiores nulla officiis eveniet libero officiis magnam', 'oil', 'Back_Boiler', 'no', 'Tarrace', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_3', 'Laborum eius consequuntur ea qui voluptas sit culpa placeat quos reprehenderit deleniti tenetur perferendis sint suscipit quis adipisicing qui', '<p>dad</p>', '6qPk2w6NtFiVUuEIA6QWOw8eKrrIZa', NULL, 3, '<p>adda</p>', NULL, '2018-06-21 16:11:17', '2018-06-21 16:11:17'),
(92, 2, '1', 'Rerum rem sint aut do vel et iste ratione', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_0', 'q8_1', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Blanditiis dolorem nihil in est quibusdam deserunt', '<p>dadada</p>', '8YHhiYJEfBObhCrsoutsL6phVXfXPQ', NULL, 2, '<p>dad</p>', NULL, '2018-06-21 16:12:51', '2018-06-21 16:12:51'),
(93, 2, '1', 'Rerum rem sint aut do vel et iste ratione', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_0', 'q8_1', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Blanditiis dolorem nihil in est quibusdam deserunt', '<p>dadada</p>', '1iBWVKTUmzGGyY8s3i33cqjwUY3HgF', NULL, 2, '<p>dad</p>', NULL, '2018-06-21 16:15:08', '2018-06-21 16:15:08'),
(94, 2, '1', 'Rerum rem sint aut do vel et iste ratione', 'oil', 'System', 'no', 'Semi-Detached', '1', 'q6_0', 'q7_0', 'q8_1', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Blanditiis dolorem nihil in est quibusdam deserunt', '<p>dadada</p>', 'dfeyxQFr3jHIs9QdUABiQXmfKIACci', NULL, 2, '<p>dad</p>', NULL, '2018-06-21 16:15:24', '2018-06-21 16:15:24'),
(95, 2, '1', 'Voluptatum accusamus temporibus libero et tempore', 'oil', 'Standard', 'yes', 'Tarrace', '1', 'q6_0', 'q7_2', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'Yes', 'q13_1', 'Id sed reiciendis in quis earum voluptatem molestiae lorem qui iste impedit facere dignissimos', '<p>daadawda</p>', 'akzkwD7DPSaKAuEW1E2CrK4IPF9hjr', NULL, 3, '<p>dadada</p>', NULL, '2018-06-21 16:17:18', '2018-06-21 16:17:18'),
(96, 2, '1', 'Voluptatum accusamus temporibus libero et tempore', 'oil', 'Standard', 'yes', 'Tarrace', '1', 'q6_0', 'q7_2', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'Yes', 'q13_1', 'Id sed reiciendis in quis earum voluptatem molestiae lorem qui iste impedit facere dignissimos', '<p>daadawda</p>', 'mrNRnFOXZ1Pm7DAu0FhvUCJmPqETl8', NULL, 3, '<p>dadada</p>', NULL, '2018-06-21 16:18:01', '2018-06-21 16:18:01'),
(97, 2, '1', 'Voluptatum accusamus temporibus libero et tempore', 'oil', 'Standard', 'yes', 'Tarrace', '1', 'q6_0', 'q7_2', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'Yes', 'q13_1', 'Id sed reiciendis in quis earum voluptatem molestiae lorem qui iste impedit facere dignissimos', '<p>daadawda</p>', 'upmeMcllGS2ZR2NXY5022ub7ATkoFH', NULL, 3, '<p>dadada</p>', NULL, '2018-06-21 16:29:45', '2018-06-21 16:29:45'),
(98, 2, '1', 'Voluptatum accusamus temporibus libero et tempore', 'oil', 'Standard', 'yes', 'Tarrace', '1', 'q6_0', 'q7_2', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'Yes', 'q13_1', 'Id sed reiciendis in quis earum voluptatem molestiae lorem qui iste impedit facere dignissimos', '<p>daadawda</p>', 'g1YnT7Qsv8BQm6YA4NC5vHV1jWq7gL', NULL, 3, '<p>dadada</p>', NULL, '2018-06-21 16:29:48', '2018-06-21 16:29:48'),
(99, 2, '1', 'Voluptatum accusamus temporibus libero et tempore', 'oil', 'Standard', 'yes', 'Tarrace', '1', 'q6_0', 'q7_2', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'Yes', 'q13_1', 'Id sed reiciendis in quis earum voluptatem molestiae lorem qui iste impedit facere dignissimos', '<p>daadawda</p>', 'ZMsog1X3KOyW0tdVeks9noLaQPiSSN', NULL, 3, '<p>dadada</p>', NULL, '2018-06-21 16:29:51', '2018-06-21 16:29:51'),
(100, 2, '1', 'Voluptatum accusamus temporibus libero et tempore', 'oil', 'Standard', 'yes', 'Tarrace', '1', 'q6_0', 'q7_2', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'Yes', 'q13_1', 'Id sed reiciendis in quis earum voluptatem molestiae lorem qui iste impedit facere dignissimos', '<p>daadawda</p>', 'khkWTxdNwIdmioQWNeT1vTow0XzCWm', NULL, 3, '<p>dadada</p>', NULL, '2018-06-21 16:29:54', '2018-06-21 16:29:54'),
(101, 2, '1', 'Voluptatum accusamus temporibus libero et tempore', 'oil', 'Standard', 'yes', 'Tarrace', '1', 'q6_0', 'q7_2', 'q8_1', 'roof', 'Sloped', 'Lowest_third', 'Yes', 'q13_1', 'Id sed reiciendis in quis earum voluptatem molestiae lorem qui iste impedit facere dignissimos', '<p>daadawda</p>', 'l5l4cpzLg25MTF2wRvtSZY8Yd7C2LB', NULL, 3, '<p>dadada</p>', NULL, '2018-06-21 16:29:56', '2018-06-21 16:29:56'),
(102, 2, '1', 'Dicta dolore est molestiae et porro iure qui quia sit', 'lpg', 'Standard', 'no', 'Detached', '1', 'q6_3', 'q7_0', 'q8_1', 'wall', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Deserunt Nam officia voluptatibus irure ea et porro quas perspiciatis maxime quis', '<p>dawdawd da</p>', 'BQPV0OqZIEcfz4R4hz3aaqVYAC33Pq', NULL, 3, '<p>dadawdaw dawd</p>', NULL, '2018-06-21 20:48:37', '2018-06-21 20:48:37'),
(103, 2, '1', 'Dicta dolore est molestiae et porro iure qui quia sit', 'lpg', 'Standard', 'no', 'Detached', '1', 'q6_3', 'q7_0', 'q8_1', 'wall', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Deserunt Nam officia voluptatibus irure ea et porro quas perspiciatis maxime quis', '<p>dawdawd da</p>', '2VKNQuxqIeGrQJ0TdTjpBsV3qvps6G', NULL, 3, '<p>dadawdaw dawd</p>', NULL, '2018-06-21 20:48:43', '2018-06-21 20:48:43'),
(104, 2, '1', 'Dicta dolore est molestiae et porro iure qui quia sit', 'lpg', 'Standard', 'no', 'Detached', '1', 'q6_3', 'q7_0', 'q8_1', 'wall', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Deserunt Nam officia voluptatibus irure ea et porro quas perspiciatis maxime quis', '<p>dawdawd da</p>', 'ub6ZOFmKETljo9JYSPRMMDTBKYhG59', NULL, 3, '<p>dadawdaw dawd</p>', NULL, '2018-06-21 20:49:22', '2018-06-21 20:49:22'),
(105, 2, '1', 'Dicta dolore est molestiae et porro iure qui quia sit', 'lpg', 'Standard', 'no', 'Detached', '1', 'q6_3', 'q7_0', 'q8_1', 'wall', 'Flat', 'Lowest_third', 'No', 'q13_1', 'Deserunt Nam officia voluptatibus irure ea et porro quas perspiciatis maxime quis', '<p>dawdawd da</p>', 'loC9HdObxK1Or9YlevcrEt8NkqkIXm', NULL, 3, '<p>dadawdaw dawd</p>', NULL, '2018-06-21 20:49:28', '2018-06-21 20:49:28'),
(106, 2, '1', 'Voluptatem Ab alias quo qui enim quis irure atque suscipit dicta velit fugiat itaque architecto eveniet', 'electricity', 'Combi', 'no', 'Detached', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_3', 'Rerum delectus in praesentium ab commodo fugit adipisci reprehenderit aspernatur quis amet labore officia quod', '<p>dawdawd da</p>', 'YDQ0MrJeWQM8her0x02OVaQsokWxmf', NULL, 2, '<p>dadawdaw dawd</p>', NULL, '2018-06-21 20:50:01', '2018-06-21 20:50:01'),
(107, 2, '1', 'Voluptatem Ab alias quo qui enim quis irure atque suscipit dicta velit fugiat itaque architecto eveniet', 'electricity', 'Combi', 'no', 'Detached', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_3', 'Rerum delectus in praesentium ab commodo fugit adipisci reprehenderit aspernatur quis amet labore officia quod', '<p>dawdawd da</p>', 'gZgbckdpPLHTt9w47maEhvVuidAwJG', NULL, 2, '<p>dadawdaw dawd</p>', NULL, '2018-06-21 20:51:12', '2018-06-21 20:51:12'),
(108, 2, '1', 'Voluptatem Ab alias quo qui enim quis irure atque suscipit dicta velit fugiat itaque architecto eveniet', 'electricity', 'Combi', 'no', 'Detached', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_3', 'Rerum delectus in praesentium ab commodo fugit adipisci reprehenderit aspernatur quis amet labore officia quod', '<p>dawdawd da</p>', '0ISujUKMsjS9MIEs36hE8mEm77AwqR', NULL, 2, '<p>dadawdaw dawd</p>', NULL, '2018-06-21 20:51:14', '2018-06-21 20:51:14'),
(109, 2, '1', 'Voluptatem Ab alias quo qui enim quis irure atque suscipit dicta velit fugiat itaque architecto eveniet', 'electricity', 'Combi', 'no', 'Detached', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_3', 'Rerum delectus in praesentium ab commodo fugit adipisci reprehenderit aspernatur quis amet labore officia quod', '<p>dawdawd da</p>', 'qxGxEiPZzXjP6M0r7H8dC2YMP2NMvs', NULL, 2, '<p>dadawdaw dawd</p>', NULL, '2018-06-21 20:51:15', '2018-06-21 20:51:15'),
(110, 2, '1', 'Nulla duis nisi veritatis quos consequatur sed iusto aliquip et duis et rem ut minim fugiat', 'lpg', 'Standard', 'yes', 'Tarrace', '1', 'q6_0', 'q7_2', 'q8_1', 'wall', 'Sloped', 'Highest_two_thirds', 'No', 'q13_3', 'Laborum aut adipisicing perferendis illum aut earum lorem voluptatem iusto aut omnis odit saepe', '<p>dawdawd da</p>', '3XWK8qspjv04DEcudz8wzpGPhDV41m', NULL, 3, '<p>dadawdaw dawd</p>', NULL, '2018-06-21 20:52:21', '2018-06-21 20:52:21'),
(111, 2, '1', 'Aut aperiam voluptatem Debitis voluptatem veniam optio ab consequatur quo', 'electricity', 'Standard', 'no', 'Tarrace', '1', 'q6_1', 'q7_2', 'q8_2', 'wall', 'Sloped', 'Highest_two_thirds', 'Yes', 'q13_2', 'Sit assumenda iure eum pariatur Necessitatibus veritatis sit proident rem ullam eu', '<p>dawdaw</p>', 'Ew0ydWWGvDx1JJTGYQRz3u4jfzeZGQ', NULL, 2, '<p>&nbsp;adawdwa</p>', NULL, '2018-06-21 20:54:38', '2018-06-21 20:54:38'),
(112, 2, '1', 'Aut aperiam voluptatem Debitis voluptatem veniam optio ab consequatur quo', 'electricity', 'Standard', 'no', 'Tarrace', '1', 'q6_1', 'q7_2', 'q8_2', 'wall', 'Sloped', 'Highest_two_thirds', 'Yes', 'q13_2', 'Sit assumenda iure eum pariatur Necessitatibus veritatis sit proident rem ullam eu', '<p>dawdaw</p>', 'CFIxlk87lgwJoKyknEWOJAsexsSPpB', NULL, 2, '<p>&nbsp;adawdwa</p>', NULL, '2018-06-21 20:54:49', '2018-06-21 20:54:49'),
(113, 2, '1', 'Aut aperiam voluptatem Debitis voluptatem veniam optio ab consequatur quo', 'electricity', 'Standard', 'no', 'Tarrace', '1', 'q6_1', 'q7_2', 'q8_2', 'wall', 'Sloped', 'Highest_two_thirds', 'Yes', 'q13_2', 'Sit assumenda iure eum pariatur Necessitatibus veritatis sit proident rem ullam eu', '<p>dawdaw</p>', 'DvnqE8XY0406sYdDrCZxrlfSRDqPVV', NULL, 2, '<p>&nbsp;adawdwa</p>', NULL, '2018-06-21 20:54:58', '2018-06-21 20:54:58'),
(114, 2, '1', 'Quod rem Nam aspernatur saepe', 'gas', 'System', 'yes', 'Detached', '1', 'q6_1', 'q7_1', 'q8_1', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_2', 'Unde officia assumenda aliqua Minus quasi eos', '<p>dwadnawkkj</p>', 'Wms4NmFiqQ4kOleCtu15LKHoaCsqIZ', '1529632611.jpg', 3, '<p>hjkhkjhkjawd</p>', NULL, '2018-06-21 20:56:51', '2018-06-21 20:56:51'),
(115, 2, '1', 'Non illum aut facilis qui tempora exercitation aut laboriosam quos sunt dolor magnam', 'electricity', 'Standard', 'no', 'Semi-Detached', '1', 'q6_1', 'q7_2', 'q8_2', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Eveniet et totam corrupti aut ut quis molestiae ea ut exercitationem', '<p>dawdawd dadaj</p>', 'tgvmxrJr6jQLHWtOz4bIz7Qscdlkzx', NULL, 2, '<p>kjhjhjk</p>', NULL, '2018-06-21 21:23:39', '2018-06-21 21:23:39'),
(116, 2, '1', 'Non illum aut facilis qui tempora exercitation aut laboriosam quos sunt dolor magnam', 'electricity', 'Standard', 'no', 'Semi-Detached', '1', 'q6_1', 'q7_2', 'q8_2', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Eveniet et totam corrupti aut ut quis molestiae ea ut exercitationem', '<p>dawdawd dadaj</p>', 'EiWIAKHzzci7ZpVYeym1fMnFBoVL2B', NULL, 2, '<p>kjhjhjk</p>', NULL, '2018-06-21 21:24:24', '2018-06-21 21:24:24'),
(117, 2, '1', 'Non illum aut facilis qui tempora exercitation aut laboriosam quos sunt dolor magnam', 'electricity', 'Standard', 'no', 'Semi-Detached', '1', 'q6_1', 'q7_2', 'q8_2', 'roof', 'Sloped', 'Lowest_third', 'No', 'q13_2', 'Eveniet et totam corrupti aut ut quis molestiae ea ut exercitationem', '<p>dawdawd dadaj</p>', 'rxMI99mBrtRGcY2Vy0srQFy4ZkGrL4', NULL, 2, '<p>kjhjhjk</p>', NULL, '2018-06-21 21:25:26', '2018-06-21 21:25:26'),
(118, 2, '1', 'Omnis qui ab commodi quis occaecat cupiditate magni quibusdam at', 'electricity', 'Back_Boiler', 'no', 'Detached', '1', 'q6_3', 'q7_1', 'q8_1', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_3', 'Rem commodi sed ratione pariatur Autem eius nulla fuga Quasi aute ipsum tempora ipsum in quis et eu', '<p>dajkhda jk</p>', 'E88HuY7iX6AnJA0KVvSpy847hqFVlJ', NULL, 2, '<p>hkj hk</p>', NULL, '2018-06-21 21:26:12', '2018-06-21 21:26:12'),
(119, 2, '1', 'Omnis qui ab commodi quis occaecat cupiditate magni quibusdam at', 'electricity', 'Back_Boiler', 'no', 'Detached', '1', 'q6_3', 'q7_1', 'q8_1', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_3', 'Rem commodi sed ratione pariatur Autem eius nulla fuga Quasi aute ipsum tempora ipsum in quis et eu', '<p>dajkhda jk</p>', 'uSjIiFB3SQxodELRK1sNunyxsthjRf', NULL, 2, '<p>hkj hk</p>', NULL, '2018-06-21 21:26:27', '2018-06-21 21:26:27'),
(120, 2, '1', 'Omnis qui ab commodi quis occaecat cupiditate magni quibusdam at', 'electricity', 'Back_Boiler', 'no', 'Detached', '1', 'q6_3', 'q7_1', 'q8_1', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_3', 'Rem commodi sed ratione pariatur Autem eius nulla fuga Quasi aute ipsum tempora ipsum in quis et eu', '<p>dajkhda jk</p>', 'dg2KcECtKQcp77i5F86teIHaBVNvTi', NULL, 2, '<p>hkj hk</p>', NULL, '2018-06-21 21:26:49', '2018-06-21 21:26:49'),
(121, 2, '1', 'Omnis qui ab commodi quis occaecat cupiditate magni quibusdam at', 'electricity', 'Back_Boiler', 'no', 'Detached', '1', 'q6_3', 'q7_1', 'q8_1', 'wall', 'Sloped', 'Lowest_third', 'No', 'q13_3', 'Rem commodi sed ratione pariatur Autem eius nulla fuga Quasi aute ipsum tempora ipsum in quis et eu', '<p>dajkhda jk</p>', 'faa8Qb8GItsNEzCBgOiMZBPoRmPqel', NULL, 2, '<p>hkj hk</p>', NULL, '2018-06-21 21:27:01', '2018-06-21 21:27:01'),
(122, 2, '1', 'Dolor eu numquam sequi voluptatum ipsum ad aliquip sed ut quia rerum', 'gas', 'System', 'no', 'Semi-Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Sloped', 'Highest_two_thirds', 'No', 'q13_1', 'Ut sed recusandae Autem rerum et hic qui blanditiis', '<p>dajkhda jk</p>', '30KClTFByz0yyfiByuSDz8fja2BKxP', NULL, 2, '<p>hkj hk</p>', NULL, '2018-06-21 21:28:05', '2018-06-21 21:28:05'),
(123, 2, '1', 'Dolor eu numquam sequi voluptatum ipsum ad aliquip sed ut quia rerum', 'gas', 'System', 'no', 'Semi-Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Sloped', 'Highest_two_thirds', 'No', 'q13_1', 'Ut sed recusandae Autem rerum et hic qui blanditiis', '<p>dajkhda jk</p>', 'Nn0NdccOZr6rYCj1OHM4THYU9qinVm', NULL, 2, '<p>hkj hk</p>', NULL, '2018-06-21 21:29:01', '2018-06-21 21:29:01'),
(124, 2, '1', 'Dolor eu numquam sequi voluptatum ipsum ad aliquip sed ut quia rerum', 'gas', 'System', 'no', 'Semi-Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Sloped', 'Highest_two_thirds', 'No', 'q13_1', 'Ut sed recusandae Autem rerum et hic qui blanditiis', '<p>dajkhda jk</p>', 'zTeEHkoginQv48VCQlPqn9fjNyedGq', NULL, 2, '<p>hkj hk</p>', NULL, '2018-06-21 21:29:31', '2018-06-21 21:29:31'),
(125, 2, '1', 'Dolor eu numquam sequi voluptatum ipsum ad aliquip sed ut quia rerum', 'gas', 'System', 'no', 'Semi-Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Sloped', 'Highest_two_thirds', 'No', 'q13_1', 'Ut sed recusandae Autem rerum et hic qui blanditiis', '<p>dajkhda jk</p>', 'iBfYNYopM7rkbMJEaAEDp0AE1WDTHZ', NULL, 2, '<p>hkj hk</p>', NULL, '2018-06-21 21:29:39', '2018-06-21 21:29:39'),
(126, 2, '1', 'Dolor eu numquam sequi voluptatum ipsum ad aliquip sed ut quia rerum', 'gas', 'System', 'no', 'Semi-Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Sloped', 'Highest_two_thirds', 'No', 'q13_1', 'Ut sed recusandae Autem rerum et hic qui blanditiis', '<p>dajkhda jk</p>', 'krOccmEOA1cCJ6jdTmy03kiDtcePWA', NULL, 2, '<p>hkj hk</p>', NULL, '2018-06-21 21:30:23', '2018-06-21 21:30:23'),
(127, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', 'q358dNidxpZYXF25iMjCtAKkwBCPgM', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:32:58', '2018-06-21 21:32:58'),
(128, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', 'zd1k7bQJDJUmphG4EnPnR1Aysvsclm', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:33:24', '2018-06-21 21:33:24'),
(129, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', 'U5iQWJ1abqnoU9MPHWOw1DP6i2LXt5', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:33:39', '2018-06-21 21:33:39'),
(130, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', 'zAZx4Z3y937MxFhbeek1FsAfEXDM6f', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:35:43', '2018-06-21 21:35:43'),
(131, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', '5Bk0Vd2Pm0CirPErWYijJ0d5tSbM8G', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:35:48', '2018-06-21 21:35:48'),
(132, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', 'NTNvAMMTigjfVPNteQfqffQxmQLWNN', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:36:17', '2018-06-21 21:36:17'),
(133, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', 'aLQYpFYWkCdGhyxLwyebXRtxfoA028', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:36:21', '2018-06-21 21:36:21'),
(134, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', 'T6d97vcENPbwUn6Xfsb0IMA4oyFjBN', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:36:36', '2018-06-21 21:36:36'),
(135, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', 'se9ZZAr5NeMOFzz515zmWGPHXbhiUu', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:37:14', '2018-06-21 21:37:14'),
(136, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', 'Tk2RrbQpYF7hoEf5DMd4cr5BfJJVJK', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:38:01', '2018-06-21 21:38:01'),
(137, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', 'RjqGzol4iMhUtC7SiyQArsZm03ZE6P', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:38:09', '2018-06-21 21:38:09'),
(138, 2, '1', 'Aut eos omnis nisi eveniet quos culpa', 'electricity', 'System', 'yes', 'Detached', '1', 'q6_2', 'q7_1', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_2', 'Deserunt nesciunt enim voluptas magnam ad', '<p>dadjhg gy</p>', 'FNN0UlgKiPg1pf8IDx9uv6iTIZVGTQ', NULL, 3, '<p>yguyguy g&nbsp;</p>', NULL, '2018-06-21 21:38:37', '2018-06-21 21:38:37'),
(139, 2, '1', 'Esse rerum quidem velit non in sit adipisci doloribus', 'electricity', 'Back_Boiler', 'no', 'Tarrace', '1', 'q6_3', 'q7_0', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_2', 'Eum similique culpa quaerat in in est in non', '<p>dawdadkl</p>', 'Oc2KqJ0rtiCK1DP9Hsxee3uRgPNocb', NULL, 3, '<p>jlkjlkda</p>', NULL, '2018-06-21 21:41:21', '2018-06-21 21:41:21'),
(140, 2, '1', 'Esse rerum quidem velit non in sit adipisci doloribus', 'electricity', 'Back_Boiler', 'no', 'Tarrace', '1', 'q6_3', 'q7_0', 'q8_2', 'wall', 'Flat', 'Highest_two_thirds', 'Yes', 'q13_2', 'Eum similique culpa quaerat in in est in non', '<p>dawdadkl</p>', 'zEuLF5MH4vXremQrxYihsRTlDtlZAJ', NULL, 3, '<p>jlkjlkda</p>', NULL, '2018-06-21 21:42:40', '2018-06-21 21:42:40'),
(141, 2, '1', 'Aute quis laboriosam do non enim maiores deserunt asperiores laborum', 'electricity', 'Standard', 'no', 'Bangalow', '1', 'q6_0', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Iste dolore dolore ea ut Nam consequat Beatae sit illum libero aliquid officia doloremque lorem', '<p>dawdadkl</p>', 'd5ienMcfgiDSpAJAW07YYyNX6pZCMb', NULL, 2, '<p>jlkjlkda</p>', NULL, '2018-06-21 21:43:17', '2018-06-21 21:43:17'),
(142, 2, '1', 'Aute quis laboriosam do non enim maiores deserunt asperiores laborum', 'electricity', 'Standard', 'no', 'Bangalow', '1', 'q6_0', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Iste dolore dolore ea ut Nam consequat Beatae sit illum libero aliquid officia doloremque lorem', '<p>dawdadkl</p>', 'sX8HY39fCwsIApqraEbhgz47K3YmJ9', NULL, 2, '<p>jlkjlkda</p>', NULL, '2018-06-21 21:44:09', '2018-06-21 21:44:09'),
(143, 2, '1', 'Aute quis laboriosam do non enim maiores deserunt asperiores laborum', 'electricity', 'Standard', 'no', 'Bangalow', '1', 'q6_0', 'q7_0', 'q8_2', 'roof', 'Flat', 'Lowest_third', 'No', 'q13_3', 'Iste dolore dolore ea ut Nam consequat Beatae sit illum libero aliquid officia doloremque lorem', '<p>dawdadkl</p>', 'ZkLJZlEvtva8LAQ27TQYLBdtqAT3eI', NULL, 2, '<p>jlkjlkda</p>', NULL, '2018-06-21 21:44:47', '2018-06-21 21:44:47'),
(144, 2, '1', 'Aperiam exercitationem est sed incididunt', 'oil', 'Back_Boiler', 'yes', 'Tarrace', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Voluptatem Maxime excepteur fugit sunt qui pariatur Neque voluptatem ipsum', '<p>dwadaw j</p>', 'EFhd6CJBiCoWiKjyCcxBMRBP86mK5m', NULL, 3, '<p>jk hh</p>', NULL, '2018-06-21 21:45:11', '2018-06-21 21:45:11');
INSERT INTO `posts` (`id`, `user_id`, `status`, `title`, `q1`, `q2`, `q3`, `q4`, `q5`, `q6`, `q7`, `q8`, `q9`, `q10`, `q11`, `q12`, `q13`, `postal_code`, `body`, `slug`, `image`, `category_id`, `address`, `deleted_at`, `created_at`, `updated_at`) VALUES
(145, 2, '1', 'Aperiam exercitationem est sed incididunt', 'oil', 'Back_Boiler', 'yes', 'Tarrace', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Voluptatem Maxime excepteur fugit sunt qui pariatur Neque voluptatem ipsum', '<p>dwadaw j</p>', 'Agr2q0A6JY9XyZDUjKA1ucrKyqoJ5A', NULL, 3, '<p>jk hh</p>', NULL, '2018-06-21 21:45:31', '2018-06-21 21:45:31'),
(146, 2, '1', 'Aperiam exercitationem est sed incididunt', 'oil', 'Back_Boiler', 'yes', 'Tarrace', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Voluptatem Maxime excepteur fugit sunt qui pariatur Neque voluptatem ipsum', '<p>dwadaw j</p>', 'UaCmXFYQrw1oSlbXuzrtuxJqbnZqd0', NULL, 3, '<p>jk hh</p>', NULL, '2018-06-21 21:45:36', '2018-06-21 21:45:36'),
(147, 2, '1', 'Aperiam exercitationem est sed incididunt', 'oil', 'Back_Boiler', 'yes', 'Tarrace', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Voluptatem Maxime excepteur fugit sunt qui pariatur Neque voluptatem ipsum', '<p>dwadaw j</p>', 'Wf8Xq2cPVQKRwhYD5PkL1vPXhGjYSp', NULL, 3, '<p>jk hh</p>', NULL, '2018-06-21 21:46:04', '2018-06-21 21:46:04'),
(148, 2, '1', 'Aperiam exercitationem est sed incididunt', 'oil', 'Back_Boiler', 'yes', 'Tarrace', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Voluptatem Maxime excepteur fugit sunt qui pariatur Neque voluptatem ipsum', '<p>dwadaw j</p>', 'Pu0ZMXReqQXbByfBt7ycegjppO1l34', NULL, 3, '<p>jk hh</p>', NULL, '2018-06-21 21:46:47', '2018-06-21 21:46:47'),
(149, 2, '1', 'Aperiam exercitationem est sed incididunt', 'oil', 'Back_Boiler', 'yes', 'Tarrace', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Voluptatem Maxime excepteur fugit sunt qui pariatur Neque voluptatem ipsum', '<p>dwadaw j</p>', 'pchZ7y2Im0UuadQZsQnuKb6h6lmuTO', NULL, 3, '<p>jk hh</p>', NULL, '2018-06-21 21:48:12', '2018-06-21 21:48:12'),
(150, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'D0z5awY8ozk60XM4qYNW9LqbZBwp3N', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:51:41', '2018-06-21 21:51:41'),
(151, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'NoztMgjM7dDdGLli4yYsrK8gnCj4Mb', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:53:29', '2018-06-21 21:53:29'),
(152, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'QX9pedys64D8IS7e74rPO9kDxxST3J', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:54:05', '2018-06-21 21:54:05'),
(153, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'h3G0iWbMDsURFOpllveFSpSMXDlrJe', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:54:21', '2018-06-21 21:54:21'),
(154, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'Ef2p3F5TiqHovkz7qY0XraXBaKSoa0', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:54:52', '2018-06-21 21:54:52'),
(155, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'Xg8jM7aO8ekUtIXEjUw4ThfZjlwxnG', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:55:25', '2018-06-21 21:55:25'),
(156, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'NTPGruKY8qwmwjTJB6eE9jWKD2u0ea', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:55:37', '2018-06-21 21:55:37'),
(157, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', '0HBuOaK4z8lpReZQOi8yriTnxQCIOW', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:55:54', '2018-06-21 21:55:54'),
(158, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'S1RGLtJPlQcWiZdM3Srq42MGa7aMQI', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:56:36', '2018-06-21 21:56:36'),
(159, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'PEeSiZmotKN7MZP3RFaNahlXmgzqd1', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:56:39', '2018-06-21 21:56:39'),
(160, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'wwVtPLQ79S4YHHcHLo7Kb8bVfTMb8N', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:56:42', '2018-06-21 21:56:42'),
(161, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'uX7Yjr7pO5Uh5C4LwzNTpXk52xYVpw', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:57:08', '2018-06-21 21:57:08'),
(162, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'CnMiYzU1XOWhLCQ5GkwVrmDVQHYRXz', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:57:28', '2018-06-21 21:57:28'),
(163, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'lNv35cqoKe3dH1mFj1vs2vnbloLcQK', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:57:46', '2018-06-21 21:57:46'),
(164, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', '1f0KIRJa1FrLmnGP1q9fzAQXWE9Drx', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:57:58', '2018-06-21 21:57:58'),
(165, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'byBf1pMDCHt6mwLznLQCP68oxAMp96', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:58:24', '2018-06-21 21:58:24'),
(166, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'GZ65jI1ZYCx8z2YAS5XhOT1Av550Fq', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:58:44', '2018-06-21 21:58:44'),
(167, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'Olitp2m4rhdOs6FL5DGvvQR4wnKXn3', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:59:11', '2018-06-21 21:59:11'),
(168, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', '0g2MSE4mnJ9KoGtOvs9NghXpSyE2nw', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 21:59:22', '2018-06-21 21:59:22'),
(169, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'LbK7f6kThb79izlsS7gL8PtGuIYzHd', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 22:00:21', '2018-06-21 22:00:21'),
(170, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'PDQcGu0f6eDevI1srnzTvwWZIFDvCs', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 22:01:02', '2018-06-21 22:01:02'),
(171, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'INNMjqlvWuEIZXxGw9CGC4oztkabFU', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 22:01:36', '2018-06-21 22:01:36'),
(172, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'iTAG7tSi1M1xLEmB3k8tQPkMq6LmCb', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 22:02:02', '2018-06-21 22:02:02'),
(173, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'kNZDnRyO6BdN2OtQ2FXJihm42VadNh', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 22:02:15', '2018-06-21 22:02:15'),
(174, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', '6Q46T3oBWm0I76LNmuoWjpjS9vPJf2', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 22:02:41', '2018-06-21 22:02:41'),
(175, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'zu9gMe4qKlKUmvhS4fMTwNRzT3q3ca', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 22:03:18', '2018-06-21 22:03:18'),
(176, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', '7lVwoel9dE7uJnZeHdyCjXmZ55yFmS', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 22:04:41', '2018-06-21 22:04:41'),
(177, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'bi6VTduPmU3w0Knt8ecXc84Dy9Mluy', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 22:04:57', '2018-06-21 22:04:57'),
(178, 2, '1', 'Impedit beatae distinctio Qui molestiae minim in illum sint aut dolor ipsa cillum nulla ut', 'gas', 'Combi', 'no', 'Tarrace', '1', 'q6_2', 'q7_1', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Molestias quam officia quas temporibus ad voluptates cupidatat ea quis excepturi quisquam aut do', '<p>dawdma jhd&nbsp;</p>', 'TR44AghGKEjlSfnZGjHuKCIpvSsONg', NULL, 2, '<p>hjk khdjkawh da</p>', NULL, '2018-06-21 22:05:06', '2018-06-21 22:05:06'),
(179, 2, '1', 'Ea ipsum consequat Deserunt natus suscipit occaecat lorem et voluptatibus', 'electricity', 'Back_Boiler', 'yes', 'Tarrace', '1', 'q6_3', 'q7_2', 'q8_2', 'wall', 'Sloped', 'Highest_two_thirds', 'No', 'q13_3', 'Ad consequuntur vel nostrud duis ipsum laborum Id doloribus incidunt eu veniam reprehenderit commodo hic earum veniam excepteur', '<p>adwkjawkj dakjh&nbsp;</p>', 'GX33EF8njNtNeDfkODrmJC7fcJzSTM', NULL, 3, '<p>jh kjhkj hkjda</p>', NULL, '2018-06-21 22:11:20', '2018-06-21 22:11:20'),
(180, 2, '1', 'Ut qui corrupti illo esse qui sunt veniam in ad deserunt fuga Mollit enim consequatur eligendi', 'gas', 'Back_Boiler', 'yes', 'Tarrace', '1', 'q6_0', 'q7_1', 'q8_1', 'wall', 'Sloped', 'Highest_two_thirds', 'No', 'q13_2', 'Et alias velit maxime eum ipsum', '<p>dawdaw</p>', 'Xj6cYGibYn6XcRz7NZpVCDrB4d7XIx', NULL, 3, '<p>&nbsp;kh</p>', NULL, '2018-06-21 22:19:41', '2018-06-21 22:19:41'),
(181, 2, '1', 'Reiciendis rerum cum perspiciatis quo dolor', 'electricity', 'Back_Boiler', 'no', 'Bangalow', '1', 'q6_2', 'q7_2', 'q8_2', 'roof', 'Flat', 'Highest_two_thirds', 'No', 'q13_1', 'Eu cum et qui quis qui voluptatem ad consequuntur eum sed impedit assumenda corrupti possimus', '<p>dawdkajljhd jdada</p>', 'w67GAAszy2J4VOv79QktRcSrAbAyBW', NULL, 3, '<p>klj kjlkj</p>', NULL, '2018-06-21 22:21:34', '2018-06-21 22:21:34');

-- --------------------------------------------------------

--
-- Table structure for table `post_media_file`
--

CREATE TABLE `post_media_file` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `filename` text COLLATE utf8_unicode_ci NOT NULL,
  `resized_name` text COLLATE utf8_unicode_ci,
  `original_name` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_media_file`
--

INSERT INTO `post_media_file` (`id`, `post_id`, `filename`, `resized_name`, `original_name`, `created_at`, `updated_at`) VALUES
(1, 179, '1529637080.jpg', NULL, NULL, '2018-06-21 22:11:20', '2018-06-21 22:11:20'),
(2, 179, '1529637080.jpg', NULL, NULL, '2018-06-21 22:11:21', '2018-06-21 22:11:21'),
(3, 180, 'Desert.jpg.jpg', NULL, NULL, '2018-06-21 22:19:42', '2018-06-21 22:19:42'),
(4, 180, 'Jellyfish.jpg.jpg', NULL, NULL, '2018-06-21 22:19:42', '2018-06-21 22:19:42'),
(5, 181, 'Chrysanthemum.jpg', NULL, NULL, '2018-06-21 22:21:34', '2018-06-21 22:21:34'),
(6, 181, 'Desert.jpg', NULL, NULL, '2018-06-21 22:21:34', '2018-06-21 22:21:34'),
(7, 181, 'Jellyfish.jpg', NULL, NULL, '2018-06-21 22:21:34', '2018-06-21 22:21:34'),
(8, 181, 'Lighthouse.jpg', NULL, NULL, '2018-06-21 22:21:34', '2018-06-21 22:21:34');

-- --------------------------------------------------------

--
-- Table structure for table `post_status`
--

CREATE TABLE `post_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_status`
--

INSERT INTO `post_status` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'pending', NULL, NULL, NULL),
(2, 'approved', NULL, NULL, NULL),
(3, 'rejected', NULL, NULL, NULL),
(4, 'OnGoing', NULL, NULL, NULL),
(5, 'completed', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE `post_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `room_dimensions`
--

CREATE TABLE `room_dimensions` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `length` double NOT NULL,
  `width` double NOT NULL,
  `height` double NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `room_dimensions`
--

INSERT INTO `room_dimensions` (`id`, `post_id`, `description`, `length`, `width`, `height`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 16, 'Molestiae in lorem perferendis aliquip', 11, 33, 22, NULL, '2018-06-18 23:39:36', '2018-06-18 23:39:36'),
(2, 16, 'Sequi at ipsam eos consectetur eligendi hic maiores adipisci accusamus ad incidunt magnam doloremque', 11, 33, 22, NULL, '2018-06-18 23:39:36', '2018-06-18 23:39:36'),
(3, 16, 'Optio quidem ea temporibus non dolorem lorem mollit voluptas laboriosam', 11, 33, 22, NULL, '2018-06-18 23:39:36', '2018-06-18 23:39:36'),
(4, 16, 'Exercitation non aliquid ut dolore est quas ullamco rerum quidem incidunt perspiciatis in dignissimos amet qui', 11, 33, 22, NULL, '2018-06-18 23:39:36', '2018-06-18 23:39:36'),
(5, 16, 'Velit unde in culpa repellendus Soluta ratione laboriosam ex ratione ut nihil fugiat voluptatum in accusantium tempore id itaque', 11, 33, 22, NULL, '2018-06-18 23:39:36', '2018-06-18 23:39:36'),
(6, 17, 'Molestiae in lorem perferendis aliquip', 11, 33, 22, NULL, '2018-06-18 23:41:39', '2018-06-18 23:41:39'),
(7, 17, 'Sequi at ipsam eos consectetur eligendi hic maiores adipisci accusamus ad incidunt magnam doloremque', 11, 33, 22, NULL, '2018-06-18 23:41:39', '2018-06-18 23:41:39'),
(8, 17, 'Optio quidem ea temporibus non dolorem lorem mollit voluptas laboriosam', 11, 33, 22, NULL, '2018-06-18 23:41:39', '2018-06-18 23:41:39'),
(9, 17, 'Exercitation non aliquid ut dolore est quas ullamco rerum quidem incidunt perspiciatis in dignissimos amet qui', 11, 33, 22, NULL, '2018-06-18 23:41:39', '2018-06-18 23:41:39'),
(10, 17, 'Velit unde in culpa repellendus Soluta ratione laboriosam ex ratione ut nihil fugiat voluptatum in accusantium tempore id itaque', 11, 33, 22, NULL, '2018-06-18 23:41:39', '2018-06-18 23:41:39'),
(11, 18, 'Molestiae in lorem perferendis aliquip', 11, 33, 22, NULL, '2018-06-18 23:43:14', '2018-06-18 23:43:14'),
(12, 18, 'Sequi at ipsam eos consectetur eligendi hic maiores adipisci accusamus ad incidunt magnam doloremque', 11, 33, 22, NULL, '2018-06-18 23:43:14', '2018-06-18 23:43:14'),
(13, 18, 'Optio quidem ea temporibus non dolorem lorem mollit voluptas laboriosam', 11, 33, 22, NULL, '2018-06-18 23:43:14', '2018-06-18 23:43:14'),
(14, 18, 'Exercitation non aliquid ut dolore est quas ullamco rerum quidem incidunt perspiciatis in dignissimos amet qui', 11, 33, 22, NULL, '2018-06-18 23:43:14', '2018-06-18 23:43:14'),
(15, 18, 'Velit unde in culpa repellendus Soluta ratione laboriosam ex ratione ut nihil fugiat voluptatum in accusantium tempore id itaque', 11, 33, 22, NULL, '2018-06-18 23:43:14', '2018-06-18 23:43:14'),
(16, 19, 'Molestiae in lorem perferendis aliquip', 11, 33, 22, NULL, '2018-06-18 23:45:13', '2018-06-18 23:45:13'),
(17, 19, 'Sequi at ipsam eos consectetur eligendi hic maiores adipisci accusamus ad incidunt magnam doloremque', 11, 33, 22, NULL, '2018-06-18 23:45:13', '2018-06-18 23:45:13'),
(18, 19, 'Optio quidem ea temporibus non dolorem lorem mollit voluptas laboriosam', 11, 33, 22, NULL, '2018-06-18 23:45:13', '2018-06-18 23:45:13'),
(19, 19, 'Exercitation non aliquid ut dolore est quas ullamco rerum quidem incidunt perspiciatis in dignissimos amet qui', 11, 33, 22, NULL, '2018-06-18 23:45:13', '2018-06-18 23:45:13'),
(20, 19, 'Velit unde in culpa repellendus Soluta ratione laboriosam ex ratione ut nihil fugiat voluptatum in accusantium tempore id itaque', 11, 33, 22, NULL, '2018-06-18 23:45:13', '2018-06-18 23:45:13'),
(21, 21, 'Molestiae in lorem perferendis aliquip', 11, 33, 22, NULL, '2018-06-18 23:47:08', '2018-06-18 23:47:08'),
(22, 21, 'Sequi at ipsam eos consectetur eligendi hic maiores adipisci accusamus ad incidunt magnam doloremque', 11, 33, 22, NULL, '2018-06-18 23:47:08', '2018-06-18 23:47:08'),
(23, 21, 'Optio quidem ea temporibus non dolorem lorem mollit voluptas laboriosam', 11, 33, 22, NULL, '2018-06-18 23:47:08', '2018-06-18 23:47:08'),
(24, 21, 'Exercitation non aliquid ut dolore est quas ullamco rerum quidem incidunt perspiciatis in dignissimos amet qui', 11, 33, 22, NULL, '2018-06-18 23:47:08', '2018-06-18 23:47:08'),
(25, 21, 'Velit unde in culpa repellendus Soluta ratione laboriosam ex ratione ut nihil fugiat voluptatum in accusantium tempore id itaque', 11, 33, 22, NULL, '2018-06-18 23:47:08', '2018-06-18 23:47:08'),
(26, 22, 'Molestiae in lorem perferendis aliquip', 11, 33, 22, NULL, '2018-06-18 23:48:31', '2018-06-18 23:48:31'),
(27, 22, 'Sequi at ipsam eos consectetur eligendi hic maiores adipisci accusamus ad incidunt magnam doloremque', 11, 33, 22, NULL, '2018-06-18 23:48:31', '2018-06-18 23:48:31'),
(28, 22, 'Optio quidem ea temporibus non dolorem lorem mollit voluptas laboriosam', 11, 33, 22, NULL, '2018-06-18 23:48:31', '2018-06-18 23:48:31'),
(29, 22, 'Exercitation non aliquid ut dolore est quas ullamco rerum quidem incidunt perspiciatis in dignissimos amet qui', 11, 33, 22, NULL, '2018-06-18 23:48:31', '2018-06-18 23:48:31'),
(30, 22, 'Velit unde in culpa repellendus Soluta ratione laboriosam ex ratione ut nihil fugiat voluptatum in accusantium tempore id itaque', 11, 33, 22, NULL, '2018-06-18 23:48:31', '2018-06-18 23:48:31'),
(31, 23, 'Molestiae in lorem perferendis aliquip', 11, 33, 22, NULL, '2018-06-18 23:48:40', '2018-06-18 23:48:40'),
(32, 23, 'Sequi at ipsam eos consectetur eligendi hic maiores adipisci accusamus ad incidunt magnam doloremque', 11, 33, 22, NULL, '2018-06-18 23:48:40', '2018-06-18 23:48:40'),
(33, 23, 'Optio quidem ea temporibus non dolorem lorem mollit voluptas laboriosam', 11, 33, 22, NULL, '2018-06-18 23:48:40', '2018-06-18 23:48:40'),
(34, 23, 'Exercitation non aliquid ut dolore est quas ullamco rerum quidem incidunt perspiciatis in dignissimos amet qui', 11, 33, 22, NULL, '2018-06-18 23:48:40', '2018-06-18 23:48:40'),
(35, 23, 'Velit unde in culpa repellendus Soluta ratione laboriosam ex ratione ut nihil fugiat voluptatum in accusantium tempore id itaque', 11, 33, 22, NULL, '2018-06-18 23:48:40', '2018-06-18 23:48:40'),
(36, 24, 'Molestiae in lorem perferendis aliquip', 11, 33, 22, NULL, '2018-06-18 23:52:39', '2018-06-18 23:52:39'),
(37, 24, 'Sequi at ipsam eos consectetur eligendi hic maiores adipisci accusamus ad incidunt magnam doloremque', 11, 33, 22, NULL, '2018-06-18 23:52:40', '2018-06-18 23:52:40'),
(38, 24, 'Optio quidem ea temporibus non dolorem lorem mollit voluptas laboriosam', 11, 33, 22, NULL, '2018-06-18 23:52:40', '2018-06-18 23:52:40'),
(39, 24, 'Exercitation non aliquid ut dolore est quas ullamco rerum quidem incidunt perspiciatis in dignissimos amet qui', 11, 33, 22, NULL, '2018-06-18 23:52:40', '2018-06-18 23:52:40'),
(40, 24, 'Velit unde in culpa repellendus Soluta ratione laboriosam ex ratione ut nihil fugiat voluptatum in accusantium tempore id itaque', 11, 33, 22, NULL, '2018-06-18 23:52:40', '2018-06-18 23:52:40'),
(41, 25, 'Molestiae in lorem perferendis aliquip', 11, 33, 22, NULL, '2018-06-18 23:55:28', '2018-06-18 23:55:28'),
(42, 25, 'Sequi at ipsam eos consectetur eligendi hic maiores adipisci accusamus ad incidunt magnam doloremque', 11, 33, 22, NULL, '2018-06-18 23:55:28', '2018-06-18 23:55:28'),
(43, 25, 'Optio quidem ea temporibus non dolorem lorem mollit voluptas laboriosam', 11, 33, 22, NULL, '2018-06-18 23:55:28', '2018-06-18 23:55:28'),
(44, 25, 'Exercitation non aliquid ut dolore est quas ullamco rerum quidem incidunt perspiciatis in dignissimos amet qui', 11, 33, 22, NULL, '2018-06-18 23:55:28', '2018-06-18 23:55:28'),
(45, 25, 'Velit unde in culpa repellendus Soluta ratione laboriosam ex ratione ut nihil fugiat voluptatum in accusantium tempore id itaque', 11, 33, 22, NULL, '2018-06-18 23:55:28', '2018-06-18 23:55:28'),
(46, 26, 'Molestiae in lorem perferendis aliquip', 11, 33, 22, NULL, '2018-06-18 23:56:08', '2018-06-18 23:56:08'),
(47, 26, 'Sequi at ipsam eos consectetur eligendi hic maiores adipisci accusamus ad incidunt magnam doloremque', 11, 33, 22, NULL, '2018-06-18 23:56:08', '2018-06-18 23:56:08'),
(48, 26, 'Optio quidem ea temporibus non dolorem lorem mollit voluptas laboriosam', 11, 33, 22, NULL, '2018-06-18 23:56:08', '2018-06-18 23:56:08'),
(49, 26, 'Exercitation non aliquid ut dolore est quas ullamco rerum quidem incidunt perspiciatis in dignissimos amet qui', 11, 33, 22, NULL, '2018-06-18 23:56:08', '2018-06-18 23:56:08'),
(50, 26, 'Velit unde in culpa repellendus Soluta ratione laboriosam ex ratione ut nihil fugiat voluptatum in accusantium tempore id itaque', 11, 33, 22, NULL, '2018-06-18 23:56:08', '2018-06-18 23:56:08'),
(51, 27, 'Molestiae in lorem perferendis aliquip', 11, 33, 22, NULL, '2018-06-18 23:56:29', '2018-06-18 23:56:29'),
(52, 27, 'Sequi at ipsam eos consectetur eligendi hic maiores adipisci accusamus ad incidunt magnam doloremque', 11, 33, 22, NULL, '2018-06-18 23:56:29', '2018-06-18 23:56:29'),
(53, 27, 'Optio quidem ea temporibus non dolorem lorem mollit voluptas laboriosam', 11, 33, 22, NULL, '2018-06-18 23:56:29', '2018-06-18 23:56:29'),
(55, 27, 'Velit unde in culpa repellendus Soluta ratione laboriosam ex ratione ut nihil fugiat voluptatum in accusantium tempore id itaque', 11, 33, 22, NULL, '2018-06-18 23:56:29', '2018-06-18 23:56:29'),
(56, 29, 'Elit repudiandae deleniti corrupti irure quos culpa dolor culpa proident consequatur Commodi molestias magnam aspernatur', 11, 33, 22, NULL, '2018-06-18 23:58:54', '2018-06-18 23:58:54'),
(57, 29, 'Dolorem ut et cupiditate occaecat', 11, 33, 22, NULL, '2018-06-18 23:58:54', '2018-06-18 23:58:54'),
(58, 29, 'Qui ea veniam at consequatur ipsum ut nihil officia quia in corporis libero fugit voluptas', 11, 33, 22, NULL, '2018-06-18 23:58:54', '2018-06-18 23:58:54'),
(59, 29, 'Magna animi qui magna elit', 11, 33, 22, NULL, '2018-06-18 23:58:54', '2018-06-18 23:58:54'),
(60, 29, 'Omnis et nesciunt dolor iusto dolor ducimus eum id inventore aut laboriosam velit', 11, 33, 22, NULL, '2018-06-18 23:58:54', '2018-06-18 23:58:54'),
(61, 29, 'Velit et occaecat qui cupidatat eu distinctio Elit est culpa aliqua Omnis corrupti animi minim nulla', 11, 33, 22, NULL, '2018-06-18 23:58:54', '2018-06-18 23:58:54'),
(62, 31, 'Ea cillum obcaecati cum voluptatem praesentium culpa ut pariatur Quia', 11, 33, 22, NULL, '2018-06-19 02:29:45', '2018-06-19 02:29:45'),
(63, 31, 'Commodo id rerum facere eu', 11, 33, 22, NULL, '2018-06-19 02:29:45', '2018-06-19 02:29:45'),
(64, 31, 'Rerum ut occaecat eveniet commodo', 11, 33, 22, NULL, '2018-06-19 02:29:45', '2018-06-19 02:29:45'),
(65, 31, 'Atque eum commodo voluptas omnis', 11, 33, 22, NULL, '2018-06-19 02:29:45', '2018-06-19 02:29:45'),
(66, 31, 'Eaque incidunt obcaecati temporibus velit consequatur qui consequuntur veritatis voluptatibus placeat', 11, 33, 22, NULL, '2018-06-19 02:29:45', '2018-06-19 02:29:45'),
(67, 32, 'Eius molestias non quia sed', 11, 33, 22, NULL, '2018-06-19 02:30:46', '2018-06-19 02:30:46'),
(68, 32, 'Eligendi reprehenderit adipisci quis ipsam', 11, 33, 22, NULL, '2018-06-19 02:30:46', '2018-06-19 02:30:46'),
(69, 32, 'Sequi sed voluptatibus autem qui vitae ut consequat Qui asperiores quibusdam qui eius magnam Nam', 11, 33, 22, NULL, '2018-06-19 02:30:46', '2018-06-19 02:30:46'),
(70, 33, 'Eius molestias non quia sed', 11, 33, 22, NULL, '2018-06-19 02:31:15', '2018-06-19 02:31:15'),
(71, 33, 'Eligendi reprehenderit adipisci quis ipsam', 11, 33, 22, NULL, '2018-06-19 02:31:15', '2018-06-19 02:31:15'),
(72, 33, 'Sequi sed voluptatibus autem qui vitae ut consequat Qui asperiores quibusdam qui eius magnam Nam', 11, 33, 22, NULL, '2018-06-19 02:31:15', '2018-06-19 02:31:15'),
(73, 34, 'Eius molestias non quia sed', 11, 33, 22, NULL, '2018-06-19 02:32:53', '2018-06-19 02:32:53'),
(74, 34, 'Eligendi reprehenderit adipisci quis ipsam', 11, 33, 22, NULL, '2018-06-19 02:32:53', '2018-06-19 02:32:53'),
(75, 34, 'Sequi sed voluptatibus autem qui vitae ut consequat Qui asperiores quibusdam qui eius magnam Nam', 11, 33, 22, NULL, '2018-06-19 02:32:53', '2018-06-19 02:32:53'),
(76, 35, 'Eius molestias non quia sed', 11, 33, 22, NULL, '2018-06-19 02:32:57', '2018-06-19 02:32:57'),
(77, 35, 'Eligendi reprehenderit adipisci quis ipsam', 11, 33, 22, NULL, '2018-06-19 02:32:57', '2018-06-19 02:32:57'),
(78, 35, 'Sequi sed voluptatibus autem qui vitae ut consequat Qui asperiores quibusdam qui eius magnam Nam', 11, 33, 22, NULL, '2018-06-19 02:32:57', '2018-06-19 02:32:57'),
(79, 36, 'Eius molestias non quia sed', 11, 33, 22, NULL, '2018-06-19 02:33:13', '2018-06-19 02:33:13'),
(80, 36, 'Eligendi reprehenderit adipisci quis ipsam', 11, 33, 22, NULL, '2018-06-19 02:33:13', '2018-06-19 02:33:13'),
(81, 36, 'Sequi sed voluptatibus autem qui vitae ut consequat Qui asperiores quibusdam qui eius magnam Nam', 11, 33, 22, NULL, '2018-06-19 02:33:13', '2018-06-19 02:33:13'),
(82, 37, 'Eius molestias non quia sed', 11, 33, 22, NULL, '2018-06-19 02:34:05', '2018-06-19 02:34:05'),
(83, 37, 'Eligendi reprehenderit adipisci quis ipsam', 11, 33, 22, NULL, '2018-06-19 02:34:05', '2018-06-19 02:34:05'),
(84, 37, 'Sequi sed voluptatibus autem qui vitae ut consequat Qui asperiores quibusdam qui eius magnam Nam', 11, 33, 22, NULL, '2018-06-19 02:34:05', '2018-06-19 02:34:05'),
(85, 38, 'Eius molestias non quia sed', 11, 33, 22, NULL, '2018-06-19 02:34:32', '2018-06-19 02:34:32'),
(86, 38, 'Eligendi reprehenderit adipisci quis ipsam', 11, 33, 22, NULL, '2018-06-19 02:34:32', '2018-06-19 02:34:32'),
(87, 38, 'Sequi sed voluptatibus autem qui vitae ut consequat Qui asperiores quibusdam qui eius magnam Nam', 11, 33, 22, NULL, '2018-06-19 02:34:32', '2018-06-19 02:34:32'),
(88, 40, 'Eius molestias non quia sed', 11, 33, 22, NULL, '2018-06-19 02:34:48', '2018-06-19 02:34:48'),
(89, 40, 'Eligendi reprehenderit adipisci quis ipsam', 11, 33, 22, NULL, '2018-06-19 02:34:48', '2018-06-19 02:34:48'),
(90, 40, 'Sequi sed voluptatibus autem qui vitae ut consequat Qui asperiores quibusdam qui eius magnam Nam', 11, 33, 22, NULL, '2018-06-19 02:34:48', '2018-06-19 02:34:48'),
(91, 42, 'Eius molestias non quia sed', 11, 33, 22, NULL, '2018-06-19 02:34:52', '2018-06-19 02:34:52'),
(92, 42, 'Eligendi reprehenderit adipisci quis ipsam', 11, 33, 22, NULL, '2018-06-19 02:34:52', '2018-06-19 02:34:52'),
(93, 42, 'Sequi sed voluptatibus autem qui vitae ut consequat Qui asperiores quibusdam qui eius magnam Nam', 11, 33, 22, NULL, '2018-06-19 02:34:52', '2018-06-19 02:34:52'),
(94, 47, 'Ea sint in adipisicing sed voluptatibus rerum reprehenderit pariatur In id animi molestiae obcaecati ea nihil aliquam', 11, 33, 22, NULL, '2018-06-19 02:35:47', '2018-06-19 02:35:47'),
(95, 47, 'Qui odit eos in ut aliquid et commodi enim sint et nihil sed', 11, 33, 22, NULL, '2018-06-19 02:35:47', '2018-06-19 02:35:47'),
(96, 48, 'Ea sint in adipisicing sed voluptatibus rerum reprehenderit pariatur In id animi molestiae obcaecati ea nihil aliquam', 11, 33, 22, NULL, '2018-06-19 02:36:32', '2018-06-19 02:36:32'),
(97, 48, 'Qui odit eos in ut aliquid et commodi enim sint et nihil sed', 11, 33, 22, NULL, '2018-06-19 02:36:32', '2018-06-19 02:36:32'),
(98, 49, 'Quia atque qui veritatis id delectus et', 11, 33, 22, NULL, '2018-06-19 02:37:31', '2018-06-19 02:37:31'),
(99, 49, 'Sapiente asperiores laboriosam incididunt sunt est quos rem proident ea alias voluptatem Distinctio Quisquam sint totam', 11, 33, 22, NULL, '2018-06-19 02:37:31', '2018-06-19 02:37:31'),
(100, 49, 'Quis dolor sint ea quas pariatur Laborum ex ipsum ipsa quos fuga Sint debitis corrupti minima id nihil similique dolore', 11, 33, 22, NULL, '2018-06-19 02:37:31', '2018-06-19 02:37:31'),
(101, 49, 'Laudantium qui qui aut occaecat et autem voluptatibus eaque dolore aut illum nulla nostrud', 11, 33, 22, NULL, '2018-06-19 02:37:31', '2018-06-19 02:37:31'),
(102, 49, 'Id officia sint omnis molestiae reprehenderit provident consequatur consequatur et esse maiores irure laborum Atque sint molestias', 11, 33, 22, NULL, '2018-06-19 02:37:31', '2018-06-19 02:37:31'),
(103, 49, 'Architecto eius voluptate tempora sunt ex quo quo debitis alias ex nisi dolor non quam exercitationem occaecat qui dolores', 11, 33, 22, NULL, '2018-06-19 02:37:31', '2018-06-19 02:37:31'),
(104, 50, 'Velit adipisci ipsa ut voluptates corrupti qui quasi nulla iste officiis velit quo est velit maiores et consequat', 11, 33, 22, NULL, '2018-06-19 02:39:17', '2018-06-19 02:39:17'),
(105, 50, 'Molestiae dolores eius aut sit est', 11, 33, 22, NULL, '2018-06-19 02:39:17', '2018-06-19 02:39:17'),
(106, 51, 'Quo velit id vel quidem', 11, 33, 22, NULL, '2018-06-19 02:53:12', '2018-06-19 02:53:12'),
(107, 51, 'In architecto molestias cumque magni est amet inventore est nihil sunt autem', 11, 33, 22, NULL, '2018-06-19 02:53:12', '2018-06-19 02:53:12'),
(108, 51, 'Dolor incidunt vel sint eu sequi ducimus sit sint sed in est', 11, 33, 22, NULL, '2018-06-19 02:53:12', '2018-06-19 02:53:12'),
(109, 54, 'Dignissimos minus voluptatibus ipsam qui possimus rem laborum', 12, 33, 22, NULL, '2018-06-19 02:56:21', '2018-06-19 02:56:21'),
(110, 58, 'Dignissimos minus voluptatibus ipsam qui possimus rem laborum', 11, 33, 22, NULL, '2018-06-19 02:56:36', '2018-06-19 02:56:36'),
(111, 59, 'Corrupti magni est maiores aut commodo nisi ut perferendis', 11, 33, 22, NULL, '2018-06-19 02:57:27', '2018-06-19 02:57:27'),
(112, 59, 'Est quibusdam fugiat et culpa in incididunt id veniam labore velit et sit et cum cupidatat sint sint', 11, 33, 22, NULL, '2018-06-19 02:57:27', '2018-06-19 02:57:27'),
(113, 59, 'Minim in facilis provident voluptatem et et commodo unde harum', 11, 33, 22, NULL, '2018-06-19 02:57:27', '2018-06-19 02:57:27'),
(114, 59, 'Et mollitia consectetur qui deserunt', 11, 33, 22, NULL, '2018-06-19 02:57:27', '2018-06-19 02:57:27'),
(115, 59, 'Laboris commodo possimus quis tempora excepturi', 11, 33, 22, NULL, '2018-06-19 02:57:27', '2018-06-19 02:57:27'),
(116, 59, 'Totam qui reprehenderit id maiores atque hic culpa et voluptatem Recusandae Temporibus ut amet', 11, 33, 22, NULL, '2018-06-19 02:57:27', '2018-06-19 02:57:27'),
(117, 60, 'Incidunt fugit itaque aut labore consequat Est dolores', 11, 33, 22, NULL, '2018-06-20 03:51:04', '2018-06-20 03:51:04'),
(118, 60, 'odkjaw jdkajw dhakwj bye bye', 11, 33, 22, NULL, '2018-06-20 03:51:04', '2018-06-20 03:51:04'),
(119, 60, 'value=\"Incidunt fugit itaque aut labore consequat Est dolores', 12, 23, 22, NULL, '2018-06-20 06:46:32', '2018-06-20 06:46:32'),
(120, 60, 'value=\"odkjaw jdkajw dhakwj bye bye', 11, 88, 99, NULL, '2018-06-20 06:46:32', '2018-06-20 06:46:32'),
(121, 54, 'hekadh kadawlkdjl ada', 10, 32, 20, NULL, '2018-06-19 02:56:21', '2018-06-19 02:56:21'),
(122, 54, 'Dignissimos minus voluptatibus ipsam qui possimus rem laborum', 12, 33, 22, NULL, '2018-06-20 14:48:42', '2018-06-20 14:48:42'),
(123, 54, 'hekadh kadawlkdjl ada', 10, 32, 20, NULL, '2018-06-20 14:48:42', '2018-06-20 14:48:42'),
(124, 61, 'adwa', 22, 22, 22, NULL, '2018-06-21 01:02:12', '2018-06-21 01:02:12'),
(125, 62, 'djkawhd adkjahwd aw', 22, 11, 11, NULL, '2018-06-21 12:09:20', '2018-06-21 12:09:20'),
(126, 63, 'dawnd ajkwd awjhdkj aw', 11, 22, 112, NULL, '2018-06-21 12:12:22', '2018-06-21 12:12:22'),
(127, 64, 'ddawdaw', 22, 22, 22, NULL, '2018-06-21 12:44:06', '2018-06-21 12:44:06'),
(128, 65, 'dawda', 1, 1, 1, NULL, '2018-06-21 12:44:38', '2018-06-21 12:44:38');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role_id`, `email`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, 'admin@super.com', '$2y$10$nhg22IRLsNC1D/mp.rvwPOgOMZhqnU0y51haE0etJIZs3eBW31n.m', '', 'onvJhetJwaRwDhWk9gPhkiTsFKCa26hfaqeg7gCoRhDbLcZvITXsiqWyJ5o8', '2018-06-14 19:46:02', '2018-06-21 12:03:05'),
(2, 'usman user', 2, 'usman@user.com', '$2y$10$8SMsw2SqUfVYlWBSzNjlouc5tJwV6u6BjeC75IRRsYEUKntt9vb6S', '', 'HBn9WdowhD6pQa2d2zaKWy41V0oajpOwbSKH4oyrCCkgfWvE1OBbyK4toSYF', '2018-06-14 20:53:32', '2018-06-20 16:26:21');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL, NULL),
(2, 'moderator', NULL, NULL, NULL),
(3, 'user', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `post_media_file`
--
ALTER TABLE `post_media_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_status`
--
ALTER TABLE `post_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_tag_post_id_foreign` (`post_id`),
  ADD KEY `post_tag_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `room_dimensions`
--
ALTER TABLE `room_dimensions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT for table `post_media_file`
--
ALTER TABLE `post_media_file`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `post_status`
--
ALTER TABLE `post_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `room_dimensions`
--
ALTER TABLE `room_dimensions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `post_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `post_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
