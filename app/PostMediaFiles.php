<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostMediaFiles extends Model
{
    protected $table = 'post_media_file';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'id', 'post_id', 'filename' , 'resized_name', 'orignal_name', 'created_at' , 'updated_at'
    ];

    public function post()
    {
    	return $this->belongsTo('App\Post');
    }
}
