<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;
use App\Tag;
use App\Category;
use App\PostMediaFiles;
use Session;
use Purifier;
use Image;
use Auth;
use App\PostStatus;
use Illuminate\Support\Facades\Response;

class PostController extends Controller
{

    private $photos_path;

    public function __construct() {
        $this->middleware('auth');
        $this->photos_path = public_path('/images');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        $role_id = Auth::user()->role_id;
        $id = Auth::user()->id;

        $postStatus = \App\PostStatus::get();
        // return $postStatus;


         if($role_id == 1) {
            $posts = Post::orderBy('id', 'desc')->paginate(10);
         }
         else {
            $posts= Post::where('user_id','=', $id)
            ->orderBy('id', 'desc')->paginate(10);
         }
        // $posts =$getAllPosts
                       
        return view('posts.index')->withPosts($posts)->with('postStatus',$postStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.create')->withCategories($categories)->withTags($tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, array(
                'job_title'         => 'required|max:255',
                // 'slug'          => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
                'category_id'   => 'required|integer',
                'job_details'          => 'required',
                'user_id' => 'required'
            ));

        // store in the database
        $post = new Post;
        $post->user_id = $request->user_id;
        $post->title = $request->job_title;
        $post->postal_code = $request->postal_code;
        $post->slug = str_random(30);
        $post->category_id = $request->category_id;
        $post->body = $request->job_details;
        $post->address = $request->job_address;
        $post->status = 1 ;
        $post->q1 = $request->q1;
        $post->q2 = $request->q2;
        $post->q3 = $request->q3;
        $post->q4 = $request->q4;
        $post->q5 = $request->q5;
        $post->q6 = $request->q6;
        $post->q7 = $request->q7;
        $post->q8 = $request->q8;
        $post->q9 = $request->q9;
        $post->q10 = $request->q10;
        $post->q11 = $request->q11;
        $post->q12 = $request->q12;
        $post->q13 = $request->q13;
        $postRoomDimensions = $request->room_dimensions;


        $post->save();

            // $product = PostMediaFiles::create($request->all());
            $files = array_filter($request->filename);
            foreach ( $files as $photo) {
                // $photo = $request->file('featured_img');

                if($photo){
                    $filename =$photo->getClientOriginalName();
                    $location = public_path('images/' . $filename);
                   Image::make($photo)->save($location);
                    $mediaFiles = new PostMediaFiles();
                    $mediaFiles->post_id = $post->id;
                    $mediaFiles->filename = $filename;
                    $mediaFiles->save();
                } 
            }   
            // return 'Upload successful!';

        //   if ($request->hasFile('featured_img')) {
        //     $image = $request->file('featured_img');
        //     $filename = time() . '.' . $image->getClientOriginalExtension();
        //     $location = public_path('images/' . $filename);
        //     Image::make($image)->resize(800, 400)->save($location);
        //     $mediaFiles->post_id = $post->id;
        //     $post->image = $filename;
        //     // $mediaFiles->save();
        //   }

        Session::flash('success', 'The blog post was successfully save!');

        //   if ($request->hasFile('featured_img3')) {
        //     $image = $request->file('featured_img3');
        //     $filename = time() . '.' . $image->getClientOriginalExtension();
        //     $location = public_path('images/' . $filename);
        //     Image::make($image)->resize(800, 400)->save($location);
        //     $mediaFiles->post_id = $post->id;
        //     $mediaFiles->filename = $filename;
        //     $mediaFiles->save();
        //   }
        //   if ($request->hasFile('featured_img4')) {
        //     $image = $request->file('featured_img4');
        //     $filename = time() . '.' . $image->getClientOriginalExtension();
        //     $location = public_path('images/' . $filename);
        //     Image::make($image)->resize(800, 400)->save($location);
        //     $mediaFiles->post_id = $post->id;
        //     $mediaFiles->filename = $filename;
        //     $mediaFiles->save();
        //   }
        //   if ($request->hasFile('featured_img5')) {
        //     $image = $request->file('featured_img5');
        //     $filename = time() . '.' . $image->getClientOriginalExtension();
        //     $location = public_path('images/' . $filename);
        //     Image::make($image)->resize(800, 400)->save($location);
        //     $mediaFiles->post_id = $post->id;
        //     $mediaFiles->filename = $filename;
        //     $mediaFiles->save();
        //   }
        //   if ($request->hasFile('featured_img6')) {
        //     $image = $request->file('featured_img6');
        //     $filename = time() . '.' . $image->getClientOriginalExtension();
        //     $location = public_path('images/' . $filename);
        //     Image::make($image)->resize(800, 400)->save($location);
        //     $mediaFiles->post_id = $post->id;
        //     $mediaFiles->filename = $filename;
        //     $mediaFiles->save();
        //   }
        //   if ($request->hasFile('featured_img7')) {
        //     $image = $request->file('featured_img7');
        //     $filename = time() . '.' . $image->getClientOriginalExtension();
        //     $location = public_path('images/' . $filename);
        //     Image::make($image)->resize(800, 400)->save($location);
        //     $mediaFiles->post_id = $post->id;
        //     $mediaFiles->filename = $filename;
        //     $mediaFiles->save();
        //   }
        //   if ($request->hasFile('featured_img8')) {
        //     $image = $request->file('featured_img8');
        //     $filename = time() . '.' . $image->getClientOriginalExtension();
        //     $location = public_path('images/' . $filename);
        //     Image::make($image)->resize(800, 400)->save($location);
        //     $mediaFiles->post_id = $post->id;
        //     $mediaFiles->filename = $filename;
        //     $mediaFiles->save();
        //   }
          
        // // save room dimensions for each post
        $temp_post_id = $post->id;
        $count_postRoomDimensions = count($postRoomDimensions['description']);
        $temp123 = \DB::transaction(function () use (&$postRoomDimensions,
                                                    &$temp_post_id,
                                                    $count_postRoomDimensions
            ){
            for($x=0; $x<$count_postRoomDimensions; $x++){
                $post_room_dimensions_temp = \App\RoomDimensions::create([
                        'post_id'=>$temp_post_id,
                        'description'=>$postRoomDimensions['description'][$x],
                        'length'=>$postRoomDimensions['length'][$x],
                        'width'=>$postRoomDimensions['width'][$x],
                        'height'=>$postRoomDimensions['height'][$x]
                    ]);
            }
        });
        // // end room dimensions save

        // // $post->tags()->sync($request->tags, false);

        Session::flash('success', 'The blog post was successfully save!');

        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $images = PostMediaFiles::where('post_id','=',$id)->get();
        // dd($images);
        $post = Post::find($id);
        $postStatus = \App\PostStatus::get();
        $rooms = \App\RoomDimensions::where('post_id','=',$id)->get();
        $postRoomsDimensions_array = [];
        if($rooms){
            foreach($rooms as $temp){
                array_push($postRoomsDimensions_array, array(
                                                        'length'=>$temp->length,
                                                        'height'=>$temp->height,
                                                        'width'=>$temp->width,
                                                        'description'=>$temp->description
                                                    ));
            }
        }
        return view('posts.show')->withPost($post)->with('postRoomsDimensions_array',$postRoomsDimensions_array)->with('images',$images)->with('postStatus', $postStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rooms = \App\RoomDimensions::where('post_id','=',$id)->get();
        // return $rooms;
        $postRoomsDimensions_array = [];
        if($rooms){
            foreach($rooms as $temp){
                array_push($postRoomsDimensions_array, array(
                                                        'length'=>$temp->length,
                                                        'height'=>$temp->height,
                                                        'width'=>$temp->width,
                                                        'description'=>$temp->description
                                                    ));
            }
            // return($postRoomsDimensions_array);

        }
        else{
            return null;
        }
        // return $postRoomsDimensions_array;
        // find the post in the database and save as a var
        $post = Post::find($id);
        // return($post);
        $categories = Category::all();
        $cats = array();
        foreach ($categories as $category) {
            $cats[$category->id] = $category->name;
        }

        $tags = Tag::all();
        $tags2 = array();
        foreach ($tags as $tag) {
            $tags2[$tag->id] = $tag->name;
        }

        $postStatus = PostStatus:: get();
        // return the view and pass in the var we previously created
        return view('posts.edit')->withPost($post)->withCategories($cats)->withTags($tags2)->with('postRoomsDimensions_array',$postRoomsDimensions_array)
        ->with('postStatus',$postStatus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate the data
        $post = Post::find($id);

        // if ($request->input('slug') == $post->slug) {
        //     $this->validate($request, array(
        //         'title' => 'required|max:255',
        //         'category_id' => 'required|integer',
        //         'body'  => 'required'
        //     ));
        // } else {
        $this->validate($request, array(
                'job_title' => 'required|max:255',
                // 'slug'  => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
                'category_id' => 'required|integer',
                'job_details'  => 'required'
            ));
        // }

        // Save the data to the database
        $post = Post::find($id);

        $post->title = $request->job_title;
        $post->slug = $request->input('slug');
        $post->category_id = $request->input('category_id');
        $post->body =$request->job_details;
        $post->address = $request->job_address;
        $post->q1 = $request->q1;
        $post->q2 = $request->q2;
        $post->q3 = $request->q3;
        $post->q4 = $request->q4;
        $post->q5 = $request->q5;
        $post->q6 = $request->q6;
        $post->q7 = $request->q7;
        $post->q8 = $request->q8;
        $post->q9 = $request->q9;
        $post->q10 = $request->q10;
        $post->q11 = $request->q11;
        $post->q12 = $request->q12;
        $post->q13 = $request->q13;
        $post->status = $request->status;
        $postRoomDimensions = $request->room_dimensions;
        // delete all rooms previously saved
        $rooms = \App\RoomDimensions::find($id);
        if ($rooms){
            $rooms->delete();
        }
        // save room dimensions for each post
        $temp_post_id = $post->id;
        $count_postRoomDimensions = count($postRoomDimensions['description']);
        $temp123 = \DB::transaction(function () use (&$postRoomDimensions,
                                                    &$temp_post_id,
                                                    $count_postRoomDimensions
            ){
            for($x=0; $x<$count_postRoomDimensions; $x++){
                $post_room_dimensions_temp = \App\RoomDimensions::create([
                        'post_id'=>$temp_post_id,
                        'description'=>$postRoomDimensions['description'][$x],
                        'length'=>$postRoomDimensions['length'][$x],
                        'width'=>$postRoomDimensions['width'][$x],
                        'height'=>$postRoomDimensions['height'][$x]
                    ]);
            }
        });
        // end room dimensions save

        $post->save();

        if (isset($request->tags)) {
            $post->tags()->sync($request->tags);
        } else {
            $post->tags()->sync(array());
        }


        // set flash data with success message
        Session::flash('success', 'This post was successfully saved.');

        // redirect with flash data to posts.show
        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->tags()->detach();

        $post->tags()->detach();

        $post->delete();

        Session::flash('success', 'The post was successfully deleted.');
        return redirect()->route('posts.index');
    }

    public function uploadfiles(Request $request){
        
        $photos = $request->file('file');

        if (!is_array($photos)) {
            $photos = [$photos];
        }

        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }

        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];
            $name = sha1(date('YmdHis') . str_random(30));
            $save_name = $name . '.' . $photo->getClientOriginalExtension();
            $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();

            Image::make($photo)
                ->resize(250, null, function ($constraints) {
                    $constraints->aspectRatio();
                })
                ->save($this->photos_path . '/' . $resize_name);

            $photo->move($this->photos_path, $save_name);

            $upload = new \App\PostMediaFiles();
            $upload->filename = $save_name;
            $upload->resized_name = $resize_name;
            $upload->original_name = basename($photo->getClientOriginalName());
            $upload->save();
        }
        return Response::json([
            'message' => 'Image saved Successfully'
        ], 200);

    }
}
