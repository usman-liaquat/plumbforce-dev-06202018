<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    // get All Users list
    public function getAllUsers(){
        $users = User::orderBy('name', 'asc')->get();
        // return $users;
        return view('users.index')->with('users', $users);
    }
}
