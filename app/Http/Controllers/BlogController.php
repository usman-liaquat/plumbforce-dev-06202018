<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use App\RoomDimensions;
use Auth;
class BlogController extends Controller
{
	public function __construct() {
        $this->middleware('auth');
    }
    
	public function getIndex() {
        $role_id = Auth::user()->role_id;
        $id  = Auth::user()->id;
        if ($role_id == 3){
            $posts = Post::where('user_id','=', $id)
            ->where('status','=','1')
            ->paginate(10);
        }
        else {
            $posts = Post::where('status','=','1')
            ->paginate(10);
        }
        
        // return $posts;
		return view('blog.index')->withPosts($posts);
	}

    public function getSingle($slug) {
    	// fetch from the DB based on slug
		$post = Post::where('slug', '=', $slug)->first();
		$rooms = \App\RoomDimensions::where('post_id','=',$post->id)->get();
        $postRoomsDimensions_array = [];
        if($rooms){
            foreach($rooms as $temp){
                array_push($postRoomsDimensions_array, array(
                                                        'length'=>$temp->length,
                                                        'height'=>$temp->height,
                                                        'width'=>$temp->width,
                                                        'description'=>$temp->description
                                                    ));
            }
		}
		
    	// return the view and pass in the post object
    	return view('blog.single')->withPost($post)->with('postRoomsDimensions_array',$postRoomsDimensions_array);;
    }
}
