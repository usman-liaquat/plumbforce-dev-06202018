<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostStatus extends Model
{
    protected $table = 'post_status';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'id', 'name', 'deleted_at' , 'created_at' , 'updated_at'
    ];

    public function post()
    {
    	return $this->belongsTo('App\Post');
    }

    public function status()
    {
    	return $this->belongsTo('App\PostStatus');
    }
}
