<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomDimensions extends Model
{
    protected $table = 'room_dimensions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'id', 'post_id', 'description' , 'length', 'width', 'height'
    ];

    public function post()
    {
    	return $this->belongsTo('App\Post');
    }
}
