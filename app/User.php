<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;
use App\UserStatus;
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status' ,
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['user_role', 'user_status'];

    public function getUserRoleAttribute()
    {
        $userRole = $this->role_id;
        // $s = explode(',', $userRole);
        $sec = Role:: find($userRole);
        // $sec = Role::where('id','=',$userRole)->pluck('name');//->flatten();
        return $sec->name;
    }

    public function getUserStatusAttribute()
    {
        $userStatus = $this->status;
        // return $userStatus;
        // $s = explode(',', $userStatus);
        $sec = UserStatus:: find($userStatus);
        // $sec = Role::where('id','=',$userStatus)->pluck('name');//->flatten();
        return $sec->name;
    }

    // public function role() {
    //     return $this->belongsTo('App\Role');
    // }

}
