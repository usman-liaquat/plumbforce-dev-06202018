<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'id', 'type_id', 'title' , 'q1', 'q2', 'q3' , 'q4' , 'q5'. 'q6', 'q7' , 'q8' , 'q9' , 'q10' , 'q11' , 'q12' , 'q13', 'status'
    ];

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function status()
    {
        return $this->belongsTo('App\PostStatus');
    }

    public function tags()
    {
    	return $this->belongsToMany('App\Tag');
    }

    public function comments()
    {
    	return $this->hasMany('App\Comment');
    }

    public function rooms()
    {
    	return $this->hasMany('App\RoomDimensions');
    }
}